USE CASES
======

## User groups

+ Admin
+ Developer
+ Executor
---

## App permissions
Permisos generales

1. Add user (?)
1. Remove user
1. List all users
### CTAFramework
Permisos de IBM

1. Add product
1. Remove product
1. Update product
1. Add Phase/Bucket
1. Remove Phase/Bucket
1. Update Phase/Bucket
1. Add Robot Framework version
   * R-Extract to add RF libraries
### Testing
Permisos relacionados con el uso del drag and drop

#### Admin interface
Probablemente se usen en vistas de admin
1. Run M-Extract
1. Add command
1. Remove command
1. Update command
1. Add product command
1. Remove product command
1. Update product command
1. Add command option
1. Remove command option
1. Update command option
1. Add advanced option
1. Remove advanced option
1. Update advanced option
1. Add Robot Framework Custom Library (RFCL)
   * Run R-Extract on custom doc file
1. Remove RFCL
   * Cascade remove on Library Keywords (?)
1. Update RFCL
1. Remove RF keyword (?)
1. Update RF keyword (?)
#### Drag and drop interface
De uso en vista de Drag and Drop
1. Add Custom Library
1. Remove Custom Library
   * Cascade remove on Custom Keywords (?)
1. Update Custom Library
1. Add Custom Keyword
1. Remove Custom Keyword
1. Update Custom Keyword
1. List all Custom keywords
1. Add Test Suite
1. Remove Test Suite
   * Cascade remove on Test Cases (?)
1. List all Test Suites
1. Add Test Suite settings
1. Remove Test Suite settings
1. Update Test Case settings
1. Add Test Case
1. Remove Test Case
1. Update Test Case
1. Run Test Case

