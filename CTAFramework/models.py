import os

import xml.etree.ElementTree as ET

from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from testing.models import Command, CommandOption, CommandRule


class Product(models.Model):
    """Model for IBM Products
    Includes method to export commands in XML format
    """
    name = models.CharField(max_length=200)
    robot_version = models.ForeignKey(
        'RobotFramework',
        related_name="products"
    )
    base_path = models.CharField(max_length=200, default='~/')
    deprecated = models.BooleanField(
        default=False,
        help_text="Check if product version already deprecated"
    )

    def __unicode__(self):
        return self.name

    def export(self):
        """ Generates link to XML export
        """
        return mark_safe(
            '<a href="%d/export_xml" target="_blank">Export XML </a>' % self.id
        )

    def export_xml(self):
        # FIX: Not PEP8 compliant
        command_list = Command.objects.filter(IBM_Product=self)
        commands = ET.Element('commands')
        product = ET.SubElement(commands, 'product')
        product.text = self.name
        product.set("version", self.version)

        for comm in command_list:
            command_rules = CommandRule.objects.filter(command=comm)
            if not command_rules:
                cmd = ET.SubElement(commands, 'command')
                cmd.set("name", comm.name)
                if comm.translate:
                    cmd.set("translate", comm.translate)
                if comm.build_range:
                    cmd.set("build_range", comm.build_range)
                if comm.workaround:
                    cmd.set("workaround", comm.workaround)
                for param in CommandOption.objects.filter(command=comm):
                    param = ET.SubElement(cmd, "param")
                    param.set("name", param.option)
                    mand = ET.SubElement(param, 'type')
                    mand.text = "mandatory" if param.mandatory else "optional"
                    if param.incompatibility:
                        incop = ET.SubElement(param, 'incompatibility')
                        incop.text = param.incompatibility
                    if param.dependent_of:
                        dependent_of = ET.SubElement(param, 'dependent')
                        dependent_of.text = param.dependent_of
                    if param.position:
                        position = ET.SubElement(param, 'position')
                        position.text = str(param.position)
                    arguments = ET.SubElement(param, 'argument')
                    if param.prefix:
                        prefix = ET.SubElement(arguments, 'prefix')
                        prefix.text = param.prefix
                    if param.suffix:
                        suffix = ET.SubElement(arguments, 'suffix')
                        suffix.text = "true"
                    if param.expected:
                        expected = ET.SubElement(arguments, 'expect')
                        expected.text = param.expected
            else:
                metacommand = ET.SubElement(commands, 'metacommand')
                metacommand.set("name", command.name)
                if command.translate:
                    metacommand.set("translate", command.translate)
                if command.build_range:
                    metacommand.set("build_range", command.build_range)
                if command.workaround:
                    metacommand.set("workaround", command.workaround)
                for r_comm in command_rules:
                    cmd = ET.SubElement(metacommand, 'command')
                    cmd.set("name", r_comm.name)
                    cmd.set("parameter", r_comm.parameter.option)
                    if r_comm.translate:
                        cmd.set("translate", r_comm.translate)
                    elif command.translate:
                        cmd.set("translate", command.translate)
                    for parameter in r_comm.mandatory_parameters.all():
                        param = ET.SubElement(cmd, 'parameter')
                        param.set("name", parameter.option)
                        mandatory = ET.SubElement(param, 'type')
                        mandatory.text = "mandatory"
                        if r_comm.incompatibility:
                            for incop in r_comm.incompatibility.split("|"):
                                if incop.split(":")[0] == parameter.name:
                                    incop = ET.SubElement(param, 'incompatibility')
                                    incop.text = incop.split(":")[1]
                                    break
                        elif parameter.incompatibility:
                            incompatibility = ET.SubElement(param, 'incompatibility')
                            incompatibility.text = parameter.incompatibility
                        if r_comm.dependencies:
                            for dependent_of in r_comm.dependencies.split("|"):
                                if dependent_of.split(":")[0] == parameter.name:
                                    dependent = ET.SubElement(param, 'dependent')
                                    dependent.text = dependent_of.split(":")[1]
                                    break
                        elif parameter.dependent_of:
                            dependent_of = ET.SubElement(param, 'dependent')
                            dependent_of.text = parameter.dependent_of
                        if parameter.position:
                            position = ET.SubElement(param, 'position')
                            position.text = str(parameter.position)
                        arguments = ET.SubElement(param, 'argument')
                        if parameter.prefix:
                            prefix = ET.SubElement(arguments, 'prefix')
                            prefix.text = parameter.prefix
                        if parameter.suffix:
                            suffix = ET.SubElement(arguments, 'suffix')
                            suffix.text = "true"
                        if parameter.expected:
                            expected = ET.SubElement(arguments, 'expect')
                            expected.text = parameter.expected
                    for parameter in r_comm.optional_parameters.all():
                        param = ET.SubElement(cmd, 'parameter')
                        param.set("name", parameter.option)
                        mandatory = ET.SubElement(param, 'type')
                        mandatory.text = "mandatory"
                        if r_comm.incompatibility:
                            for incompatibility in r_comm.incompatibility.split("|"):
                                if incompatibility.split(":")[0] == parameter.name:
                                    incompatible = ET.SubElement(param, 'incompatibility')
                                    incompatible.text = incompatibility.split(":")[1]
                                    break
                        elif parameter.incompatibility:
                            incompatibility = ET.SubElement(param, 'incompatibility')
                            incompatibility.text = parameter.incompatibility
                        if r_comm.dependencies:
                            for dependent_of in r_comm.dependencies.split("|"):
                                if dependent_of.split(":")[0] == parameter.name:
                                    dependent = ET.SubElement(param, 'dependent')
                                    dependent.text = dependent_of.split(":")[1]
                                    break
                        elif parameter.dependent_of:
                            dependent_of = ET.SubElement(param, 'dependent')
                            dependent_of.text = parameter.dependent_of
                        if parameter.position:
                            position = ET.SubElement(param, 'position')
                            position.text = str(parameter.position)
                        arguments = ET.SubElement(param, 'argument')
                        if parameter.prefix:
                            prefix = ET.SubElement(arguments, 'prefix')
                            prefix.text = parameter.prefix
                        if parameter.suffix:
                            suffix = ET.SubElement(arguments, 'suffix')
                            suffix.text = "true"
                        if parameter.expected:
                            expected = ET.SubElement(arguments, 'expect')
                            expected.text = parameter.expected
        tree = ET.ElementTree(commands)
        tree.write("%sproducts/%s_%s.xml" % (
            settings.MEDIA_ROOT,
            self.name,
            self.version)
        )

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
        # Call the "real" save() method.
        super(Product, self).save(*args, **kwargs)
        directory = self.base_path
        if not os.path.exists(directory):
            os.makedirs(directory)


class Phase(models.Model):
    """ Model to represent Jenkings testing phase
    """
    name = models.CharField(max_length=140)
    product = models.ForeignKey(Product, related_name='phases')

    def __unicode__(self):
        return "%s's %s" % (self.product, self.name)


class RobotFramework(models.Model):
    """Model for Robot Framework versioning
    Specifically designed to manage Keyword versions for Products
    """
    version = models.CharField(max_length=14)

    def __unicode__(self):
        return "Robot Framework %s" % self.version

    class Meta:
        verbose_name = "Robot Framework version"
        verbose_name_plural = "Robot Framework versions"
