import os
import sys
from CTAFramework.models import Product, CommandOption, Command as IBMCommand
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from optparse import make_option
import xml.etree.ElementTree as ET


class Command(BaseCommand):
    args = '--path <path> --check_existing_commands <True>'
    help = """import_commands

        Usage:
        python manage.py (or ./manage.py) import_commands --path <path_of_xml> (--check_existing_commands True)

        Will take an xml with next format
        <commands>
            <product version="version">product_name</product>
            <command name="name" translate="command_translation" build_range="build_range" workaround="workaround"> (build_range is optional, can be a range or comma separated values of builds, same for workaround, it depends of build_range exist or not)
                <parameter name="name">
                    <type>optional/mandatory</type>
                    <incompatibility>parameter1, parameter2, ..., parameterN</incompatibility> (this is optional)
                    <position>number</position> (this is optional)
                    <argument>
                        <prefix>prefix</prefix> (optional, can be --name, -name, etc.)
                        <suffix>true</suffix> (optional, only true is accepted)
                        <expect>expected values comma separated</expect> (optional)
                    </argument>
                </parameter>
                ...
            </command>
            ...
        </commands>

        Use check_existing_commands for verify if command exists in other product versions
        """

    option_list = BaseCommand.option_list + (
        make_option('--path',
            action=None,
            dest='path',
            default=None,
            help='Use for import xml file with commands'),
        make_option('--check_existing_commands',
            action=None,
            dest='check_existing_commands',
            default=False,
            help=''),
        )

    def handle(self, *args, **options):
        """
        import_command

        Usage:
        python manage.py (or ./manage.py) import_commands --path <path_of_xml>  (--check_existing_commands True)

        Will take an xml with next format
        <commands>
            <product version="version">product_name</product>
            <command name="name" translate="command_translation" build_range="build_range"> (build_range is optional, can be a range or comma separated values of builds)
                <parameter name="name">
                    <type>optional/mandatory</type>
                    <position>number</position>
                    <position>number</position> (this is optional)
                    <argument>
                        <prefix>prefix</prefix> (optional, can be --name, -name, etc.)
                        <suffix>true</suffix> (optional, only true is accepted)
                        <expect>expected values comma separated</expect> (optional)
                    </argument>
                </parameter>
                ...
            </command>
            ...
        </commands>

        Use check_existing_commands for verify if command exists in other product versions
        """
        path = options['path']
        if not path:
            # Verify path included in the call of this command
            raise CommandError("Please specify an xml path in the --path parameter")
        if not os.path.isfile(path):
            # Verify path is valid
            raise CommandError("Invalid xml path")
        try:
            tree = ET.parse(path)
            root = tree.getroot()
        except:
            # Verify xml is valid file
            raise CommandError("Invalid xml file")

        product = command = commandoption = version = created = None
        for num, child in enumerate(root):
            if num==0 and not child.tag == "product":
                # Checking xml have product
                raise CommandError("Invalid xml, product is missing")
            elif num==0:
                # Check produc's version, if not set default as "base"
                version = child.attrib.get("version")
                if not version:
                    version = "base"
                product, created = Product.objects.get_or_create(name=child.text, version=version)
                if created:
                    print "Creating %s %s product" % (child.text, version)
                    product.save()
                else:
                    print "Updating %s %s product" % (child.text, version)
            else:
                if not child.tag == "command":
                    # Validating xml have commands after product
                    if created:
                        product.delete()
                    raise CommandError("Invalid xml, command tag expected")
                else:
                    command_name = child.attrib.get("name")
                    if not command_name:
                        raise CommandError("Invalid xml, command name missing")
                    command_translation = child.attrib.get("translate")
                    command_build = child.attrib.get("build_range")
                    imported = False
                    if options['check_existing_commands']:
                        # Search for existing command
                        products = Product.objects.filter(name = product.name)
                        product_list = []
                        for p in products:
                            # Search accorss al versions of the product for a command with the same features
                            if command_build:
                                existing_command = IBMCommand.objects.filter(IBM_Product = p, name = command_name, build_range = command_build, translate = command_translation if command_translation else None)
                            else:
                                existing_command = IBMCommand.objects.filter(IBM_Product = p, name = command_name, translate = command_translation if command_translation else None)
                            if existing_command:
                                # If exist, ask for use existing
                                import_command = raw_input("%s command already exist in %s %s do you want use existing? (y/*)" % (command_name, p.name, p.version))
                                if import_command.lower() == "y":
                                    # Import the existing command
                                    existing_command[0].IBM_Product.add(product)
                                    print "Importing %s command from %s %s" % (command_name, p.name, p.version)
                                    imported = True
                                    break
                    if not options['check_existing_commands'] or not imported:
                        # Import from XML
                        command_exist = False
                        command_list = IBMCommand.objects.filter(name = command_name, build_range = command_build if command_build else None, translate = command_translation if command_translation else None)
                        for c in command_list:
                            # Check if command already exist for current product (we will update)
                            if product in c.IBM_Product.all():
                                command_exist = True
                                command = c
                                break
                        else:
                            # Create new command
                            command = IBMCommand.objects.create(name = command_name, build_range = command_build if command_build else None, translate = command_translation if command_translation else None)
                            command.IBM_Product.add(product)
                        if not command_exist:
                            command.save()
                            print "Creating %s command for %s %s" % (command_name, product.name, product.version)
                        else:
                            print "Updating %s command for %s %s" % (command_name, product.name, product.version)
                        for parameter in child:
                            # Check all command options/parameters
                            if not parameter.tag == "parameter":
                                if command_created:
                                    command.delete()
                                if created:
                                    product.delete()
                                raise CommandError("Invalid xml, parameter tag expected")
                            else:
                                parameter_name = parameter.attrib.get("name")
                                mandatory = suffix = False
                                position = -1
                                prefix = expect = incompatibility = dependancy = ""
                                for parameter_option in parameter:
                                    # Check command parameters
                                    if parameter_option.tag == "type":
                                        mandatory = parameter_option.text == "mandatory"
                                    elif parameter_option.tag == "dependent":
                                        dependancy = parameter_option.text
                                    elif parameter_option.tag == "incompatibility":
                                        incompatibility = parameter_option.text
                                    elif parameter_option.tag == "position":
                                        try:
                                            position = int(parameter_option.text)
                                        except:
                                            print "Invalid position %s will not included" % parameter_option.text
                                    elif parameter_option.tag == "argument":
                                        # Check command arguments
                                        for argument in parameter_option:
                                            if argument.tag == "prefix":
                                                prefix = argument.text
                                            elif argument.tag == "suffix":
                                                suffix = True
                                            elif argument.tag == "expect":
                                                expect = argument.text
                                            else:
                                                print "ignoring %s argument" % (argument.tag,)
                                                pass # ignore invalid arguments
                                    else:
                                        print "ignoring %s parameter option" % (parameter_option.tag,)
                                        pass # ignore invalid parameters
                                # Get or create command option objetc
                                command_option, command_option_created = CommandOption.objects.get_or_create(option = parameter_name, command = command)
                                if command_option_created:
                                    print "Adding %s parameter" % parameter_name
                                    command_option.save()
                                else:
                                    print "Updating %s parameter" % parameter_name
                                command_option.prefix = prefix if prefix else None
                                command_option.suffix = suffix
                                command_option.dependent_of = dependancy if dependancy else None
                                command_option.expected = expect if expect else None
                                command_option.mandatory = mandatory
                                command_option.position = position if position > -1 else None
                                command_option.incompatibility = incompatibility
                                command_option.save()
