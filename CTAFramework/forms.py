from django import forms
from django.core.exceptions import ValidationError
from IBMauth.bluepages import get_user_info
from IBMauth.models import JoinProjectRequest


class JoinProjectForm(forms.ModelForm):

    class Meta:
        model = JoinProjectRequest
        exclude = ()
        widgets = {
            'user': forms.TextInput()
        }
        error_messages = {
            'user': {
                'invalid_choice': "User doesn't exist in IBM Bluepages",
            },
        }


class RExtractForm(forms.Form):
    docfile = forms.FileField(
        label='Documentation file',
        help_text='Upload a Robot Framework documentation zip file'
    )

