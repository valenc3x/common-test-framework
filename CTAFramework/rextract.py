import json
import re
import zipfile
import urllib2
from urllib2 import Request

from CTAFramework.models import RobotFramework
from testing.models import RobotLibrary, RobotKeyword, KeywordOption


class RExtract:
    """ R-Extract class to fetch and save Robot Framework keywords
    """
    def __init__(self, filename, extra_urls=None):
        """ R-Extract initialization
            Opens zip file and get libs names and inner paths for later parsing
        """
        self.version = filename.name.split('-')[2].rsplit('.', 1)[0]
        self.zip = zipfile.ZipFile(filename)
        self.r_version, rfcreated = RobotFramework.objects.get_or_create(
            version=self.version
        )
        self.libraries = list()
        self.extra_libraries = list()
        for path in self.zip.namelist():
            if re.search(r'/libraries/', path):
                name = path.split('/')[-1].split('.')[0]
                self.libraries.append({
                    'name': name,
                    'lib_page': self.zip.open(path).readlines()
                })
        for url in extra_urls:
            req = Request(url)
            res = urllib2.urlopen(req)
            name = url.split('/')[-1].split('.')[0]
            self.libraries.append({
                'name': name,
                'lib_page': res.readlines()
            })

    def _lib_parser(self, lib):
        """ Parses a Robot library from a json formated string
            format is expected to be like the Robot Framework 3.0 page
        """
        libdoc = False
        for line in lib['lib_page']:
            # libdoc is the JS variable where the docs are stored.
            #  webpage renders doc tables based on this json
            if re.search(r'libdoc =', line):
                libdoc = True
                line = re.sub(r'\\x3c', '<', line)
                line = re.sub(r';', '', line)
                lib_dict = json.loads(line.split('=', 1)[1])
                rbt_lib, lcreated = RobotLibrary.objects.update_or_create(
                    name=lib_dict['name'],
                    robot_version=self.r_version,
                )
                for keyword in lib_dict['keywords']:
                    rbt_keyw, rcreated = RobotKeyword.objects\
                        .update_or_create(
                            name=keyword['name'],
                            library=rbt_lib,
                            help_text=keyword['shortdoc'],
                            docs=keyword['doc']
                        )
                    for arg in keyword['args']:
                        arg_split = arg.split('=')
                        arg_name = arg_split[0]
                        default = arg_split[1] if len(arg_split) > 1 else ''
                        keyw_opt, ocreated = KeywordOption.objects\
                            .update_or_create(
                                name=arg_name,
                                keyword=rbt_keyw,
                                default_value=default
                            )
        # endfor
        return libdoc

    def run_r_extract(self):
        """ Runs R Extract with loaded libraries
        """
        for lib in self.libraries:
            print "Running parser for %s" % lib['name']
            result = self._lib_parser(lib)
            if result:
                print "Library %s added" % lib['name']
                continue
            else:
                return False
        return True
