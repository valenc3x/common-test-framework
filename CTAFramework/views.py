# coding: utf-8
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from IBMauth.models import User

from .forms import JoinProjectForm
from .models import *
from .utils import only_post_function


def index(request):
    return render(request, 'CTAFramework/index.html', {
            "products": Product.objects.filter(deprecated=False),
            "title": "Common Test Automation",
    })


# TODO: Weird index functions. still not working
@only_post_function
def get_commands(request, id_get):
    product = Product.objects.get(id=id_get)
    commands = Command.objects.filter(IBM_Product=product)
    html = ""
    for c in commands:
        html += "<option value=" + ('m_' if c.commandrule_set.all() else 'c_') +\
            str(c.id) + ">" + c.name + "</option>"
    if not commands:
        html = "<option value=0>There are no commands for selected product</options>"
    return HttpResponse(html)


@only_post_function
def get_command_info(request, id_command):
    command = Command.objects.get(id=id_command)
    html = "<div>Command: %s " % (command.name,)
    if command.translate:
        html += "<br> Translate: %s " % (command.translate,)
    if command.build_range:
        html += "<br> Build Range: %s " % (command.build_range,)
    if command.help_text:
        html += "<br> %s " % (command.help_text,)
    html += "</div>"
    return HttpResponse(html)


# TODO: Change this to template render method. This is ugly and not very smart
@only_post_function
def get_command(request, id_command):
    command = Command.objects.get(id=id_command)
    parameters = CommandOption.objects.filter(command=command)
    html = u""
    if parameters:
        html += u'<thead><tr> '
        html += u'<th class="ibm-sort"><a href="#sort"><span>Parameter</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th id="default-order" class="ibm-numeric ibm-sort"><a href="#sort"><span>Position</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Mandatory</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Prefix</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Suffix</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th>Expected</th>'
        html += u'<th>Dependancy</th>'
        html += u'<th>Incompatibility</th>'
        html += u'<th>Help Text</th>'
        html += u'</tr></thead>'
        for p in parameters:
            html += u'<tr><td class="ibm-table-row ibm-sort-column">' + p.option + u'</td>'
            html += u'<td class="ibm-numeric ibm-sort-column">' + (unicode(p.position) if p.position else '') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/' + unicode(p.mandatory) + u'.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.prefix if p.prefix else u'') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/' + unicode(p.suffix) + u'.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.expected if p.expected else u'') + u'</td>'
            html += u'<td class="ibm-sort-column">' + (p.dependent_of if p.dependent_of else u'<img src=/media/img/False.gif />') + u'</td>'
            html += u'<td>' + (p.incompatibility if p.incompatibility else '') + u'</td>'
            if p.help_text:
                help_text = p.help_text.replace(u'\u2018', u"'")
                help_text = p.help_text.replace(u'\u2019', u"'")
                help_text = p.help_text.replace(u'\u201c', u'"')
                help_text = p.help_text.replace(u'\u201d', u'"')
                help_text = p.help_text.replace(u'\u2013', u'-')
            else:
                help_text = ''
            try:
                html += u'<td>' + help_text + u'</td>'
            except:
                assert False, help_text
    return HttpResponse(html)


@only_post_function
def get_metacommand(request, id_command):
    rules = CommandRule.objects.filter(command=id_command)
    html = ""
    for r in rules:
        html += "<option value=%d>%s - %s</option>" % \
            (r.id, r.name. r.parameter.option)
    return HttpResponse(html)


@only_post_function
def get_rule_info(request, id_rule):
    rule = CommandRule.objects.get(id=id_rule)
    html = "<div>Command: %s - %s" % (rule.name, rule.parameter.option)
    if rule.translate or rule.command.translate:
        html += "<br> Translate: %s " % \
            (rule.translate or rule.command.translate)
    if rule.command.build_range:
        html += "<br> Build Range: %s " % (rule.command.build_range,)
    if rule.help_text or rule.command.help_text:
        html += "<br> %s " % (rule.help_text or rule.command.help_text,)
    html += "</div>"
    return HttpResponse(html)


# TODO: Change this to template render method. This is ugly and not very smart
@only_post_function
def get_rule(request, id_rule):
    rule = CommandRule.objects.get(id=id_rule)
    html = u""
    mandatory = rule.mandatory_parameters.all()
    optional = rule.optional_parameters.all()
    if mandatory or optional:
        html += u'<thead><tr> '
        html += u'<th class="ibm-sort"><a href="#sort"><span>Parameter</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th id="default-order" class="ibm-numeric ibm-sort"><a href="#sort"><span>Position</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Mandatory</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Prefix</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th class="ibm-sort"><a href="#sort"><span>Suffix</span><span class="ibm-icon">&nbsp;</span></a></th>'
        html += u'<th>Expected</th>'
        html += u'<th>Dependancy</th>'
        html += u'<th>Incompatibility</th>'
        html += u'<th>Help Text</th>'
        html += u'</tr></thead>'
        for p in mandatory:
            html += u'<tr><td class="ibm-table-row ibm-sort-column">' + p.option + u'</td>'
            html += u'<td class="ibm-numeric ibm-sort-column">' + (unicode(p.position) if p.position else '') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/True.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.prefix if p.prefix else u'') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/' + unicode(p.suffix) + u'.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.expected if p.expected else u'') + u'</td>'
            html += u'<td class="ibm-sort-column">' + (p.dependent_of if p.dependent_of else u'<img src=/media/img/False.gif />') + u'</td>' #TODO
            html += u'<td>' + (p.incompatibility if p.incompatibility else '') + u'</td>' #TODO
            if p.help_text:
                help_text = p.help_text.replace(u'\u2018', u"'")
                help_text = p.help_text.replace(u'\u2019', u"'")
                help_text = p.help_text.replace(u'\u201c', u'"')
                help_text = p.help_text.replace(u'\u201d', u'"')
                help_text = p.help_text.replace(u'\u2013', u'-')
            else:
                help_text = ''
            try:
                html += u'<td>' + help_text + u'</td>'
            except:
                assert False, help_text

        for p in optional:
            html += u'<tr><td class="ibm-table-row ibm-sort-column">' + p.option + u'</td>'
            html += u'<td class="ibm-numeric ibm-sort-column">' + (unicode(p.position) if p.position else '') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/True.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.prefix if p.prefix else u'') + u'</td>'
            html += u'<td class="ibm-sort-column"><img src=/media/img/' + unicode(p.suffix) + u'.gif /></td>'
            html += u'<td class="ibm-sort-column">' + (p.expected if p.expected else u'') + u'</td>'
            html += u'<td class="ibm-sort-column">' + (p.dependent_of if p.dependent_of else u'<img src=/media/img/False.gif />') + u'</td>' #TODO
            html += u'<td>' + (p.incompatibility if p.incompatibility else '') + u'</td>' #TODO
            if p.help_text:
                help_text = p.help_text.replace(u'\u2018', u"'")
                help_text = p.help_text.replace(u'\u2019', u"'")
                help_text = p.help_text.replace(u'\u201c', u'"')
                help_text = p.help_text.replace(u'\u201d', u'"')
                help_text = p.help_text.replace(u'\u2013', u'-')
            else:
                help_text = ''
            try:
                html += u'<td>' + help_text + u'</td>'
            except:
                assert False, help_text

    return HttpResponse(html)


# Auth help views
def access_request(request):
    if request.method == 'POST':
        request.POST = request.POST.copy()
        request.POST['user'] = _user_validation(
            request.POST['user'],
            skip_validation=True
        )
        print request.POST
        form = JoinProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/thanks')
    else:
        form = JoinProjectForm()
    return render(request, 'access_request.html', {
        'form': form,
        'title': "Access Request"
    })


def thanks(request):
    return render(request, 'thanks.html', {
        'title': "Thanks for your access request"
    })


def _user_validation(user, skip_validation=False):
    try:
        user_info = get_user_info(user)
        # info is valid, create user
        has_ibm_info = True
    except:
        has_ibm_info = False
    try:
        user_info["firstname"]
    except:
        # nevermind
        has_ibm_info = False
    if has_ibm_info or skip_validation:
        user_info = {
            'firstname': user,
            'lastname': ''
        }
        user, ucreated = User.objects.get_or_create(username=user)
        user.first_name = user_info["firstname"]
        user.last_name = user_info["lastname"]
        user.save()
        user = user.id
    return user
