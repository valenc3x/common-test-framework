from threading import Thread
from django.http import Http404


def run_on_thread(function):
    """ Decorator to run function on a separate thread
    """
    def wrapper(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return wrapper


def only_post_function(f):
    """ Decorator for verify a view that only works with POST calls
    """
    def wrapper(*args, **kargs):
        if type(args[0]).__name__ == "WSGIRequest":
            if not args[0].POST:
                raise Http404
        else:
            if not args[1].POST:
                raise Http404
        return f(*args, **kargs)
    return wrapper


# to avoid exception crashes
def catch_em_all(f):
    """ Decorator to catch every Exception raised to avoid crashes and
        print all local variables. Intended for debugging purposes only
    """
    def wrapper(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except Exception, e:
            print locals()
            print e
    return wrapper
