#!/usr/bin/python2
import IBMadmin as admin
from IBMauth.models import JoinProjectRequest

from .admin_managers.joinprojectrequest import JoinProjectRequestAdmin
from .admin_managers.product import ProductAdmin
from .admin_managers.robotframework import RobotFrameworkAdmin
from .models import *


# CTAFramework
admin.site.register(Phase)
admin.site.register(JoinProjectRequest, JoinProjectRequestAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(RobotFramework, RobotFrameworkAdmin)
