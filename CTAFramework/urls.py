from django.conf.urls import patterns, include, url
from .views import *

urlpatterns = patterns(
    '',
    url(r'^$', index, name="home"),
    url(r'access_request$', access_request, name="access_request"),
    url(r'thanks$', thanks, name="thanks"),

    # ajax endpoint for commands docs. prob not working
    url(r'get_commands/(?P<id_get>\d+)$', get_commands),
    url(r'get_command/(?P<id_command>\d+)$', get_command),
    url(r'get_command_info/(?P<id_command>\d+)$', get_command_info),
    url(r'get_metacommand/(?P<id_command>\d+)$', get_metacommand),
    url(r'get_rule/(?P<id_rule>\d+)$', get_rule),
    url(r'get_rule_info/(?P<id_rule>\d+)$', get_rule_info),
)
