from django.conf.urls import url

from IBMadmin import IBMAdmin
from IBMauth.models import JoinProjectRequest


class JoinProjectRequestAdmin(IBMAdmin):
    """ Admin class to manage join request to product teams.
        Cannot be added or deleted via permissions
    """
    list_display = ['user', 'group', 'approved']
    list_display_links = None
    actions = ['grant_access', 'revoke_access']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    # TODO: Send confirmation email
    def grant_access(self, request, queryset):
        """ Grants access to every user in the queryset
        """
        for obj in queryset:
            obj.user.groups.add(obj.group)
            obj.approved = True
            obj.save()
    grant_access.short_description = "Grant selected access requests"

    # TODO: Send confirmation email
    def revoke_access(self, request, queryset):
        """ Revokes access for every user-group in the queryset
        """
        for obj in queryset:
            obj.user.groups.remove(obj.group)
            obj.approved = False
            obj.save()
    revoke_access.short_description = "Revoke selected access requests"

    def get_actions(self, request):
        actions = super(JoinProjectRequestAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions
