from CTAFramework.forms import RExtractForm
from CTAFramework.rextract import RExtract
from django.conf.urls import patterns, url
from django.shortcuts import redirect, render
from IBMadmin import IBMAdmin


class RobotFrameworkAdmin(IBMAdmin):
    list_display_links = None

    def get_urls(self):
        urls = super(RobotFrameworkAdmin, self).get_urls()
        new_urls = [
            url(r'^rextract/$', self.r_extract, name="r_extract"),
        ]
        return new_urls + urls

    def r_extract(self, request):
        """ Handles R Extract view
        """
        template_vars = {
            'title': 'R-Extract',
        }
        if request.method == "POST":
            doc_urls = request.POST.getlist('urls')
            rex_form = RExtractForm(request.POST, request.FILES)
            rex = RExtract(request.FILES['docfile'], doc_urls)
            if rex.run_r_extract():
                return redirect('/admin/CTAFramework/robotframework/')
            else:
                rex_form.add_error('docfile', 'Bad documentation format')
        else:
            rex_form = RExtractForm()
        template_vars['form'] = rex_form
        return render(
            request, 'admin/CTAFramework/robotframework/r_extract.html',
            template_vars
        )
