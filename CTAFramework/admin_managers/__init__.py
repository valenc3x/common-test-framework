from django.contrib.admin import SimpleListFilter, StackedInline


class IsDeprecatedFilter(SimpleListFilter):
    title = 'Deprecated'
    parameter_name = 'deprecated__exact'

    def lookups(self, request, model_admin):
        return (
            (None, 'No'),
            ('1', 'Yes'),
            ('all', 'All'),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(deprecated=True)
        elif self.value() is None:
            return queryset.filter(deprecated=False)
