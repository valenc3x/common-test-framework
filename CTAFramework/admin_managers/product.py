import os
import xml.etree.ElementTree as ET

from datetime import datetime

from django.conf import settings
from django.conf.urls import url
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, render

from . import IsDeprecatedFilter
from CTAFramework.models import *
from CTAFramework.utils import only_post_function
from IBMadmin import IBMAdmin
from testing.models import *


class ProductAdmin(IBMAdmin):
    list_display = ['name', 'export']
    list_filter = [IsDeprecatedFilter]

    def get_urls(self):
        urls = super(ProductAdmin, self).get_urls()
        new_urls = [
            url(r'^(?P<id>\d+)/export_xml$',
                self.export_xml, name="export_xml"),
            url(r'^(?P<id>\d+)/get_products$',
                self.get_other_products, name="get_products"),
            url(r'^(?P<id>\d+)/get_commands/(?P<id_get>\d+)$',
                self.get_commands, name="get_commands"),
            url(r'^(?P<id>\d+)/import_commands$',
                self.import_commands, name="export_xml"),
            url(r'^import/$',
                self.import_product, name="import"),
        ]
        return new_urls + urls

        def get_queryset(self, request):
            """Limit Pages to those that belong to the request's user.
            Can only be used if object has foreign key to product
            """
            qs = super(GroupFilteredAdmin, self).get_queryset(request)
            group_names = request.user.groups.values_list('name', flat=True)
            if request.user.is_superuser or "Administrator" in group_names:
                return qs
            groups = []
            if "Test" not in self.model.__name__:
                # Harcoded group names may lead to maintainability issues
                for g in group_names:
                    if " Developer" in g:
                        groups.append(g.split(" - Developer")[0])
                return qs.filter(product__name__in=groups)
            else:
                return qs.filter(product__name__in=group_names)

    def handle_xml_file(self, f):
        # FIX: function not PEP8 compliant
        """ Import xml files from admin site.
        """
        if not f.name.lower().endswith(".xml"):
            # Code Error 1
            return "Invalid XML", 1
        f_name = settings.MEDIA_ROOT + 'products/tmp' + \
            datetime.now().isoformat() + f.name
        with open(f_name, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        try:
            tree = ET.parse(f_name)
            root = tree.getroot()
        except:
            # Code Error 2
            return "Invalid XML format", 2
        product = command = commandoption = version = created = None
        messages = []
        for num, child in enumerate(root):
            if num == 0 and not child.tag == "product":
                # Checking xml have product
                # Code Error 3 no product found
                return "Invalid xml, product is missing", 3
            elif num == 0:
                # Check produc's version, if not set default as "base"
                version = child.attrib.get("version")
                if not version:
                    version = "base"
                product, created = Product.objects.get_or_create(
                    name=child.text,
                    version=version
                )
                if created:
                    messages.append(
                        "Creating %s %s product<br/>" % (child.text, version)
                    )
                    product.save()
                else:
                    messages.append(
                        "Updating %s %s product<br/>" % (child.text, version)
                    )
            else:
                if child.tag == "metacommand":
                    cmd_name = child.attrib.get("name")
                    if not cmd_name:
                        if created:
                            product.delete()
                        # Code error 5, command name missing
                        return "Invalid xml, metacommand name missing", 5
                    cmd_translation = child.attrib.get("translate")
                    cmd_build = child.attrib.get("build_range")
                    cmd_workaround = child.attrib.get("workaround")
                    # TODO: in future check the option of use existing commands
                    # Import from XML
                    cmd_exist = False
                    cmd_list = Command.objects.filter(
                        product=product,
                        name=cmd_name,
                        build_range=cmd_build if cmd_build else '',
                        workaround=cmd_workaround if cmd_workaround else '',
                        translate=cmd_translation if cmd_translation else ''
                    )
                    if cmd_list:
                        cmd_exist = True
                        command = cmd_list[0]
                        messages.append(
                            "Updating %s metacommand for %s %s<br/>" %
                            (cmd_name, product.name, product.version)
                        )
                    else:
                        # Create new command
                        command = Command.objects.create(
                            name=cmd_name,
                            build_range=cmd_build if cmd_build else '',
                            workaround=cmd_workaround if cmd_workaround else '',
                            translate=cmd_translation if cmd_translation else ''
                        )
                        command.product.add(product)
                        command.save()
                        messages.append(
                            "Creating %s metacommand for %s %s<br/>" %
                            (cmd_name, product.name, product.version)
                        )
                    for rule in child:
                        if rule.tag == "command":
                            rule_name = rule.attrib.get("name")
                            rule_translation = rule.attrib.get("translate")
                            rule_parameter = rule.attrib.get("parameter")
                            for parameter in rule:
                                param_name = parameter.attrib.get("name")
                                cmd_option, co_created = CommandOption.\
                                    objects.get_or_create(
                                        option=param_name,
                                        command=command
                                    )
                                cr_created = False
                                try:
                                    cmd_rule = CommandRule.\
                                        objects.get(
                                            name=rule_name,
                                            command=command)
                                except:
                                    cmd_rule, cr_created = CommandRule.\
                                        objects.get_or_create(
                                            name=rule_name,
                                            command=command,
                                            parameter=cmd_option)
                                if cr_created:
                                    messages.append(
                                        "Creating %s command rule "
                                        "for %s parameter<br/>" %
                                        (rule_name, param_name))
                                else:
                                    messages.append(
                                        "Updating %s command rule "
                                        "for %s parameter<br/>" %
                                        (rule_name, param_name))
                                if co_created:
                                    cmd_option.mandatory = False
                                    cmd_option.incompatibility = None
                                    cmd_option.dependent_of = None
                                    cmd_option.suffix = False
                                    cmd_option.prefix = None
                                    cmd_option.expected = None
                                    cmd_option.dependencies = ""
                                for param_opt in parameter:
                                    if param_opt.tag == "type":
                                        if param_opt.text == "mandatory":
                                            cmd_rule.mandatory_parameters.\
                                                add(cmd_option)
                                        else:
                                            cmd_rule.optional_parameters.\
                                                add(cmd_option)
                                    elif param_opt.tag == "dependent":
                                        if cmd_rule.dependencies:
                                            cmd_rule.dependencies += u"|",\
                                                param_name + u":" + \
                                                param_opt.text
                                        else:
                                            cmd_rule.dependencies = \
                                                param_name + u":" + \
                                                param_opt.text
                                    elif param_opt.tag == "incompatibility":
                                        if cmd_rule.incompatibility:
                                            cmd_rule.incompatibility += u"|",\
                                                param_name + u":" + \
                                                param_opt.text
                                        else:
                                            cmd_rule.incompatibility = \
                                                param_name + u":" + \
                                                param_opt.text
                                    elif param_opt.tag == "position":
                                        try:
                                            cmd_option.position = \
                                                int(param_opt.text)
                                        except:
                                            messages.append("<span class='warning'>Invalid position %s will not included</span><br/>" % param_opt.text)
                                    elif param_opt.tag == "argument":
                                        # Check command arguments
                                        for argument in param_opt:
                                            if argument.tag == "prefix":
                                                cmd_option.prefix = argument.text
                                            elif argument.tag == "suffix":
                                                cmd_option.suffix = True
                                            elif argument.tag == "expect":
                                                cmd_option.expected = argument.text
                                            else:
                                                messages.append("<span class='warning'>ignoring %s argument</span><br/>" % (argument.tag,))
                                    else:
                                        messages.append("<span class='warning'>ignoring %s parameter option</span><br/>" % (param_opt.tag,))
                                cmd_option.save()
                                cmd_rule.save()
                        else:
                            if cmd_created:
                                command.delete()
                            if created:
                                product.delete()
                            # Code error 5.5, no commands
                            err_msg = "Invalid xml, command tag expected "\
                                "after metacommand"
                            return err_msg, 5.5
                elif child.tag == "command":
                    cmd_name = child.attrib.get("name")
                    if not cmd_name:
                        if created:
                            product.delete()
                        # Code error 5, command name missing
                        return "Invalid xml, command name missing", 5
                    cmd_translation = child.attrib.get("translate")
                    cmd_build = child.attrib.get("build_range")
                    cmd_workaround = child.attrib.get("workaround")
                    # TODO: in future check the option of use existing commands
                    # Import from XML
                    cmd_exist = False
                    cmd_list = Command.objects.filter(
                        product=product,
                        name=cmd_name,
                        build_range=cmd_build if cmd_build else '',
                        workaround=cmd_workaround if cmd_workaround else '',
                        translate=cmd_translation if cmd_translation else ''
                    )
                    if cmd_list:
                        # Check if command already exist for current product
                        cmd_exist = True
                        command = cmd_list[0]
                        messages.append(
                            "Updating %s command for %s %s<br/>"
                            % (cmd_name, product.name, product.version))
                    else:
                        # Create new command
                        command = Command.objects.create(
                            name=cmd_name,
                            build_range=cmd_build if cmd_build else '',
                            workaround=cmd_workaround if cmd_workaround else '',
                            translate=cmd_translation if cmd_translation else ''
                        )
                        command.product.add(product)
                        command.save()
                        messages.append(
                            "Creating %s command for %s %s<br/>"
                            % (cmd_name, product.name, product.version))
                    for parameter in child:
                        # Check all command options/parameters
                        if not parameter.tag == "parameter":
                            if cmd_created:
                                command.delete()
                            if created:
                                product.delete()
                            # Code error 6, no parameters
                            return "Invalid xml, parameter tag expected", 6
                        else:
                            param_name = parameter.attrib.get("name")
                            mandatory = suffix = False
                            position = -1
                            prefix = expect = incompatibility = dependancy = ""
                            for param_option in parameter:
                                # Check command parameters
                                if param_option.tag == "type":
                                    mandatory = param_option.text == "mandatory"
                                elif param_option.tag == "dependent":
                                    dependancy = param_option.text
                                elif param_option.tag == "incompatibility":
                                    incompatibility = param_option.text
                                elif param_option.tag == "position":
                                    try:
                                        position = int(param_option.text)
                                    except:
                                        messages.append("<span class='warning'>Invalid position %s will not included</span><br/>" % param_option.text)
                                elif param_option.tag == "argument":
                                    # Check command arguments
                                    for argument in param_option:
                                        if argument.tag == "prefix":
                                            prefix = argument.text
                                        elif argument.tag == "suffix":
                                            suffix = True
                                        elif argument.tag == "expect":
                                            expect = argument.text
                                        else:
                                            messages.append("<span class='warning'>ignoring %s argument</span><br/>" % (argument.tag,))
                                else:
                                    messages.append("<span class='warning'>ignoring %s parameter option</span><br/>" % (param_option.tag,))
                            # Get or create command option objetc
                            cmd_option, cmd_option_created = CommandOption.\
                                objects.get_or_create(
                                    option=param_name,
                                    command=command
                                )
                            if cmd_option_created:
                                messages.append(
                                    "Adding %s parameter<br/>" % param_name
                                )
                                cmd_option.save()
                            else:
                                messages.append(
                                    "Updating %s parameter<br/>" % param_name
                                )
                            cmd_option.prefix = prefix or None
                            cmd_option.suffix = suffix
                            cmd_option.dependent_of = dependancy or None
                            cmd_option.expected = expect or None
                            cmd_option.mandatory = mandatory
                            cmd_option.position = position if position > -1 else None
                            cmd_option.incompatibility = incompatibility
                            cmd_option.save()
                else:
                    # Validating xml have commands after product
                    if created:
                        product.delete()
                    # Code error 4, command tag not found
                    return "Invalid xml, command tag expected", 4
        os.system("rm " + f_name)
        messages.append("Success...")
        return messages, 0

    def import_product(self, request):
        errors = ""
        messages = []
        if request.POST:
            if not request.FILES:
                errors = "XML file is required"
            else:
                messages, code = self.handle_xml_file(request.FILES['xml_file'])
                if code != 0:
                    errors, messages = messages, []
        opts = self.model._meta
        # Change the content element. Use template render
        template_vars = {
            "title": "Import from XML",
            "opts": opts,
            "errors": errors,
            "import_messages": messages,
            "content": '''<br/>
                &lt;commands&gt;<br/>
                &nbsp;&nbsp;&lt;product version="version"&gt;product_name&lt;/product&gt;<br/>
                &nbsp;&nbsp;&lt;command name="name" translate="command_translation" build_range="build_range" workaround="workaround"&gt; (build_range is optional, can be a range or comma separated values of builds, same for workaround, it depends of build_range exist or not)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&lt;parameter name="name"&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;type&gt;optional/mandatory&lt;/type&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;incompatibility&gt;parameter1, parameter2, ..., parameterN&lt;/incompatibility&gt; (this is optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;position&gt;number&lt;/position&gt; (this is optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;prefix>prefix&lt;/prefix&gt; (optional, can be --name, -name, etc.)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;suffix>true&lt;/suffix&gt; (optional, only true is accepted)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;expect&gt;expected values comma separated&lt;/expect&gt; (optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/argument&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&lt;/parameter&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;...<br/>
                &nbsp;&nbsp;&lt;/command&gt;<br/>
                &nbsp;&nbsp;&lt;metacommand name="command_name"&gt;<br/>
                &nbsp;&nbsp;&nbsp;&lt;command name="name" parameter="differentianting parameter" translate="command_translation" build_range="build_range" workaround="workaround"&gt; (build_range is optional, can be a range or comma separated values of builds, same for workaround, it depends of build_range exist or not)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;parameter name="name"&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;type&gt;optional/mandatory&lt;/type&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;incompatibility&gt;parameter1, parameter2, ..., parameterN&lt;/incompatibility&gt; (this is optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;position&gt;number&lt;/position&gt; (this is optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;prefix>prefix&lt;/prefix&gt; (optional, can be --name, -name, etc.)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;suffix>true&lt;/suffix&gt; (optional, only true is accepted)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;expect&gt;expected values comma separated&lt;/expect&gt; (optional)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/argument&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/parameter&gt;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
                &nbsp;&nbsp;&nbsp;&lt;/command&gt;<br/>
                &nbsp;&nbsp;&lt;/metacommand&gt;<br/>
                &nbsp;&nbsp;...<br/>
                &lt;/commands&gt;
                '''
            }
        return render(
            request,
            'admin/CTAFramework/product/import_product.html',
            template_vars
        )

    def export_xml(self, request, id):
        product = Product.objects.get(id=id)
        product.export_xml()
        path = settings.MEDIA_URL + "products/" + product.name + "_" + \
            product.version + ".xml"
        return redirect(path)

    @only_post_function
    def get_other_products(self, request, id):
        product = Product.objects.get(id=id)
        products = Product.objects.filter(~Q(id=id) & Q(name=product.name))
        html = "<option value=0>Select another product's version</options>"
        for p in products:
            html += "<option value=" + str(p.id) + ">" + p.version + "</option>"
        if not products:
            html = "<option value=0>There are no other product's versions</options>"
        return HttpResponse(html)

    @only_post_function
    def get_commands(self, request, id, id_get):
        product = Product.objects.get(id=id_get)
        current_product = Product.objects.get(id=id)
        commands = Command.objects.filter(product=product)
        # Exclude commands with same name that exist in current product
        commands_excluded = commands.exclude(
            name__in=map(lambda x: x.name, Command.objects.filter(
                product=current_product
            ))
        )
        html = ""
        for c in commands_excluded:
            html += "<option value=" + str(c.id) + ">" + c.name + "</option>"
        if not commands:
            html = "<option value=0>There are no commands for selected product</options>"
        elif not commands_excluded:
            html = "<option value=0>Commands with same name already exist in %s %s</options>" % (current_product.name, current_product.version)
        return HttpResponse(html)

    @only_post_function
    def import_commands(self, request, id):
        command_list = request.POST.getlist("commands[]")
        if not command_list:
            raise Http404
        product = Product.objects.get(id=id)
        for command_id in command_list:
            try:
                command = Command.objects.get(id=int(command_id))
                command.product.add(product)
            except:
                return HttpResponse("Error: invalid commands")
        else:
            return HttpResponse(
                "Success: commands was imported into %s" %
                product.__unicode__()
            )

