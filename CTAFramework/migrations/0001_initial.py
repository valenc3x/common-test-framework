# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Phase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('version', models.CharField(help_text=b'Example: 1.0.1', max_length=10)),
                ('deprecated', models.BooleanField(default=False, help_text=b'Check if product version already deprecated')),
            ],
            options={
                'ordering': ['name', '-version'],
                'verbose_name': 'IBM Product',
                'verbose_name_plural': 'IBM Products',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RobotFramework',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.CharField(max_length=14)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='product',
            name='robot_version',
            field=models.ForeignKey(related_name='products', to='CTAFramework.RobotFramework'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='phase',
            name='product',
            field=models.ForeignKey(related_name='phases', to='CTAFramework.Product'),
            preserve_default=True,
        ),
    ]
