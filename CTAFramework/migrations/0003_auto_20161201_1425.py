# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0002_auto_20161129_1205'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='robotframework',
            options={'verbose_name': 'Robot Framework version', 'verbose_name_plural': 'Robot Framework versions'},
        ),
    ]
