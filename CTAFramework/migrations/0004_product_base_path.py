# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0003_auto_20161201_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='base_path',
            field=models.CharField(default=b'~/', max_length=200),
            preserve_default=True,
        ),
    ]
