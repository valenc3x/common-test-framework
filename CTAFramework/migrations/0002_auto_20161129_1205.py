# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['name'], 'verbose_name': 'IBM Product', 'verbose_name_plural': 'IBM Products'},
        ),
        migrations.RemoveField(
            model_name='product',
            name='version',
        ),
    ]
