import copy
import re

from django import http
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.sites import AdminSite, NotRegistered
from django.contrib.auth import authenticate, login, REDIRECT_FIELD_NAME
from django.core import urlresolvers
from django.http import HttpResponseRedirect, Http404
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy, ugettext as _
from django.views.decorators.cache import never_cache

from IBMauth.shortcuts import get_admin_templates
from IBMauth.models import Contact

ERROR_NO_PERMISSION = (
    "You do not have permission to access to the administration site."
    " Please contact your Program Manager to request access.")
LOGIN_FORM_KEY = 'this_is_the_login_form'


# Custom Admin Site is needed to allow seemless login with bluepage authentication
# and to not store any passwords locally
class IBMAdminSite(AdminSite):
    """
    An AdminSite object encapsulates an instance of the Django admin
    application, ready to be hooked in to your URLconf. Models are registered
    with the AdminSite using the register() method, and the root() method can
    then be used as a Django view function that presents a full admin interface
    for the collection of registered models.

    class AdminSite(name=None)
    from django.contrib.auth import authenticate, login
    A Django administrative site is represented by an instance of
    django.contrib.admin.sites.AdminSite; by default, an instance of this class
    is created as django.contrib.admin.site and you can register your models
    and ModelAdmin instances with it.

    If you'd like to set up your own administrative site with custom behavior,
    however, you're free to subclass AdminSite and override or add anything
    you like. Then, simply create an instance of your AdminSite subclass
    (the same way you'd instantiate any other Python class), and register your
    models and ModelAdmin subclasses with it instead of using the default.

    When constructing an instance of an AdminSite, you are able to provide a
    unique instance name using the name argument to the constructor. This
    instance name is used to identify the instance, especially when reversing
    admin URLs. If no instance name is provided, a default instance name of
    admin will be used.
    """
    index_template = get_admin_templates('admin', 'index.html')
    login_template = get_admin_templates('admin', 'login.html')
    # logout_template = get_admin_templates('admin', 'logout.html')
    # password_change_template = None
    # password_change_done_template = None

    """ The models and apps to show on the admin index page """
    index_blocks = [
        'productcfa',
        'midmarketcfa',
        'industrycfa',
        'rolecfa',
        'productasset',
        'roleasset',
        'salesplay',
        'offering',
        'bestpractice',
        'pages',
        'user',
    ]

    def __init__(self, **kwargs):
        super(IBMAdminSite, self).__init__(self, **kwargs)
        # This is needed to prevent admin errors in templates
        self.root_path = '/admin/'

    @never_cache
    def login(self, request, extra_context=None):
        """
        Displays the login form for the given HttpRequest.
        """
        if request.method == 'GET' and self.has_permission(request):
            # Already logged-in, redirect to admin index
            index_path = reverse('admin:index', current_app=self.name)
            return HttpResponseRedirect(index_path)

        from django.contrib.auth.views import login
        from django.contrib.admin.forms import AdminAuthenticationForm
        context = dict(self.each_context(),
            title=_('Log in'),
            app_path=request.get_full_path(),
        )
        if (REDIRECT_FIELD_NAME not in request.GET and
                REDIRECT_FIELD_NAME not in request.POST):
            context[REDIRECT_FIELD_NAME] = request.get_full_path()
        context.update(extra_context or {})

        defaults = {
            'extra_context': context,
            'current_app': self.name,
            'authentication_form': self.login_form or AdminAuthenticationForm,
            'template_name': self.login_template or 'admin/login.html',
        }
        return login(request, **defaults)

    def password_change(self, request):
        return None

    def password_change_done(self, request):
        return None

    def reregister(self, model, model_admin=None):
        try:
            self.unregister(model)
        except NotRegistered:
            pass
        if model_admin is None:
            self.register(model)
        else:
            self.register(model, model_admin)

    def get_admin_for_model(self, model):
        return self._registry.get(model, None)

    def index(self, request, extra_context=None):
        """
        Displays the main admin index page, which lists all of the installed
        apps that have been registered in this site.
        """
        blocks = []
        extra_context = extra_context or {}
        index_blocks = self._flatten_index_blocks()
        for model, model_admin in self._registry.items():
            app_label = model._meta.app_label
            has_module_perms = request.user.has_module_perms(app_label)
            module_name = model.__name__.lower()
            if has_module_perms:
                # Add module to admin index
                if module_name in index_blocks:
                    name = model._meta.verbose_name_plural
                    name = name[0].upper() + name[1:]
                    name = getattr(model_admin, 'block_title', name)
                    blocks.append({
                        'name': name,
                        'url': mark_safe('%s/%s/' % (app_label, module_name)),
                        'order': index_blocks.index(module_name),
                    })
                # Add app to admin index
                try:
                    app_title, app_label = self.\
                        index_blocks[index_blocks.index(app_label)]
                except ValueError:
                    app_title = app_label.title()
                app_not_added = app_title not in [x['name'] for x in blocks]
                if app_label in index_blocks and app_not_added:
                    blocks.append({
                        'name': app_title,
                        'url': app_label + '/',
                        'order': index_blocks.index(app_label),
                    })
        blocks = sorted(blocks, key=lambda block: block['order'])
        extra_context.update({'blocks': blocks})
        return super(IBMAdminSite, self).index(
            request,
            extra_context=extra_context)
    index = never_cache(index)

    def _flatten_index_blocks(self):
        """ Allow custom names for index_blocks """
        index_blocks = []
        for block in self.index_blocks:
            if isinstance(block, (list, tuple)):
                block = block[1]
            index_blocks.append(block)
        return index_blocks

site = IBMAdminSite()


class AuditAdmin(admin.ModelAdmin):
    """
    Mixin to be used with AuditMixin
    """
    def f_created_by(self, obj):
        return obj.created_by or 'Unknown'
    f_created_by.short_description = 'Created by'

    def f_updated_by(self, obj):
        return obj.updated_by or 'Unknown'
    f_updated_by.short_description = 'Updated by'

    def save_model(self, request, obj, form, change):
        instance = form.save(commit=False)
        if not instance.id:
            instance.created_by = request.user.email
        instance.updated_by = request.user.email
        instance.save()
        form.save_m2m()
        return instance

    def save_formset(self, request, form, formset, change):
        def set_user(instance):
            instance.user = request.user
            instance.save()
        return formset.save()


def create_fieldsets(fieldkeys):
    """
    Create fieldsets for Admin from a fieldkey object
    """

    f = [(k, fieldkeys[k]) for k in fieldkeys.keys()]
    fieldsets = copy.deepcopy(f)
    fieldsets.sort(key=lambda x: x[1].get('order', 50))
    for key, obj in fieldsets:
        if 'order' in obj:
            del obj['order']

    return fieldsets


class IBMAdmin(admin.ModelAdmin):
    """
    Base admin class that adds additional functionality to
    the base model admin
    """

    def get_fieldsets(self, request, obj=None):
        if hasattr(self, 'fieldkeys'):
            return create_fieldsets(getattr(self, 'fieldkeys'))
        return super(IBMAdmin, self).get_fieldsets(request, obj)

    def _is_changelist_url(self, referer_url):
        changelist_url = self.model._meta.app_label + "/" + \
            self.model._meta.module_name \
            + "/(\?([a-z0-9_%,-]+=[a-z0-9_&%,-]+)*)?"
        changelist_url_regex = re.compile(changelist_url, re.IGNORECASE)
        return changelist_url_regex.search(referer_url) is not None

    def add_view(self, request, form_url='', extra_context=None):
        """ Django view for the model instance addition page """
        if "_exit" in request.POST:
            return self._get_exit_redirect(request)

        return super(IBMAdmin, self).add_view(
            request,
            form_url,
            extra_context or {})

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """ Django view for the model instance edition page """
        if "_exit" in request.POST:
            return self._get_exit_redirect(request)
        try:
            int(object_id)
        except:
            raise Http404
        return super(IBMAdmin, self).change_view(
            request,
            object_id,
            form_url,
            extra_context or {})

    def _get_exit_redirect(self, request):
        """ Determine the correct redirect URL after exiting without saving """
        try:
            redirect = request.session[self.model._meta.app_label][self.model._meta.module_name]
        except KeyError:
            redirect = '../'
        return HttpResponseRedirect(redirect)

    def history_view(self, request, object_id, extra_context=None):
        """ Django view for the page that shows the modification history
            for a given model instance
        """
        return super(IBMAdmin, self).history_view(
            request,
            object_id,
            extra_context
        )


# Weird but (kind of)effective
class GroupFilteredAdmin(IBMAdmin):
    def get_queryset(self, request):
        """Limit Pages to those that belong to the request's user.
        Can only be used if object has foreign key to product
        """
        qs = super(GroupFilteredAdmin, self).get_queryset(request)
        group_names = request.user.groups.values_list('name', flat=True)
        if request.user.is_superuser or "Administrator" in group_names:
            return qs
        groups = []
        if "Test" not in self.model.__name__:
            # Harcoded group names may lead to maintainability issues
            for g in group_names:
                if " Developer" in g:
                    groups.append(g.split(" - Developer")[0])
            return qs.filter(product__name__in=groups)
        else:
            return qs.filter(product__name__in=group_names)

    def has_delete_permission(self, request, obj=None):
        group_names = request.user.groups.values_list('name', flat=True)
        # Another harcoding red flag
        if request.user.is_superuser or "Administrator" in group_names:
            return True
        if obj:
            try:
                # checks for 'author' field. this needs standarized naming
                if obj.author == request.user:
                    return True
            except:
                return False
        return False


class MultiDBModelAdmin(IBMAdmin):
    """
    See http://docs.djangoproject.com/en/1.2/topics/db/multi-db/#using-managers-with-multiple-databases
    """
    using = 'default'

    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.using)

    def get_queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super(MultiDBModelAdmin, self).\
            get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).\
            formfield_for_foreignkey(
                db_field,
                request=request,
                using=self.using,
                **kwargs)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).\
            formfield_for_manytomany(
                db_field,
                request=request,
                using=self.using,
                **kwargs)

    class Meta:
        abstract = True


class GlobalModelAdmin(MultiDBModelAdmin):
    """
    Models that are stored in the central guidancehub database should
    extend this admin class in their admin.py file
    """
    # A handy constant for the name of the alternate database.
    using = 'global'
