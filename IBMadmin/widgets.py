import os

from django.conf import settings
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe


class AdminImageWidget(AdminFileWidget):
    """
    A FileField Widget that displays an image instead of a file path
    if the current file is an image.
    """
    def render(self, name, value, attrs=None):
        output = [u'<p>']
        file_name = str(value)
        if file_name:
            if os.path.exists(os.path.join(settings.MEDIA_ROOT, file_name)):
                # is image
                absolute_url = os.path.join(settings.MEDIA_URL, file_name)
                output.append(u'<img src="%s" alt="%s" /><br/><br/>%s' % (absolute_url, file_name, u'Change: '))
            else:
                # not image
                output.append(u'Change: ')
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        output.append(u'</p>')
        return mark_safe(u''.join(output))