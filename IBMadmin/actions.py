"""
Custom admin actions for hub-specific models
"""

from django.contrib import messages
from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.admin.util import model_ngettext
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db.models.fields import CharField, SlugField


def publish(modeladmin, request, queryset):
    """
    Admin action that publishes all selected objects
    """
    if not modeladmin.has_change_permission(request):
        raise PermissionDenied
    queryset.update(published=True)
    messages.add_message(
        request,
        messages.SUCCESS,
        "Successfully published %(count)d %(items)s." % {
            'count': len(queryset),
            'items': model_ngettext(modeladmin.opts, len(queryset))
        })


def unpublish(modeladmin, request, queryset):
    """
    Admin action that unpublishes all selected objects
    """
    if not modeladmin.has_change_permission(request):
        raise PermissionDenied
    queryset.update(published=False)
    messages.add_message(
        request,
        messages.SUCCESS,
        "Successfully unpublished %(count)d %(items)s." % {
            'count': len(queryset),
            'items': model_ngettext(modeladmin.opts, len(queryset))
        })


def make_staff(modeladmin, request, queryset):
    """
    Admin action that make staff all selected objects
    """
    if not modeladmin.has_change_permission(request):
        raise PermissionDenied
    queryset.update(is_staff=True)
    messages.add_message(
        request,
        messages.SUCCESS,
        "Successfully marked as staff %(count)d %(items)s." % {
            'count': len(queryset),
            'items': model_ngettext(modeladmin.opts, len(queryset))
        })


def add_to_groups(modeladmin, request, queryset):
    """
    Admin action that add users to a specific group
    """
    if not modeladmin.has_change_permission(request):
        raise PermissionDenied
    for user in queryset:
        user.groups = [1, 2]
        user.save()
    messages.add_message(
        request,
        messages.SUCCESS,
        "Successfully added to 2 groups %(count)d %(items)s." % {
            'count': len(queryset),
            'items': model_ngettext(modeladmin.opts, len(queryset))
        })


def clone_objects(objects, request, title_fieldnames):
    """
    Helper method for duplicate_selected admin action
    """
    # TODO: this logic should be done in the Model class, and call that method
    # TODO: this does not copy M2M associations
    def clone(from_object, title_fieldnames):
        args = dict([(fld.name, getattr(from_object, fld.name))
                    for fld in from_object._meta.fields
                    if fld is not from_object._meta.pk])
        for field in from_object._meta.fields:
            if field.name in title_fieldnames:
                if isinstance(field, SlugField):
                    args[field.name] = getattr(from_object, field.name) + "-copy"
                elif isinstance(field, CharField):
                    args[field.name] = getattr(from_object, field.name) + " (copy)"
                else:
                    model = from_object.__class__.__name__
                    raise Exception(u'could not duplicate object because of '+field.name)
            elif field.name == 'published':
                args[field.name] = False

        return from_object.__class__.objects.create(**args)

    if not hasattr(objects, '__iter__'):
        objects = [objects]

    # We always have the objects in a list now
    objs = []
    for object in objects:
        obj = clone(object, title_fieldnames)
        obj.save()
        objs.append(obj)

        # Save an entry in the admin log
        object_id = obj.id
        object_repr = obj._meta.verbose_name
        action_flag = ADDITION
        content_type_id = ContentType.objects.\
            get(model=obj.__class__.__name__.lower(),
                app_label=obj._meta.app_label).id
        change_message = "Duplicated as %s" % obj.__unicode__()
        user_id = request.user.id
        LogEntry.objects.log_action(
            user_id=user_id,
            content_type_id=content_type_id,
            object_id=object_id,
            object_repr=object_repr,
            action_flag=action_flag,
            change_message=change_message
        )
    return objs


def duplicate(modeladmin, request, queryset):
    """
    Duplicate all selected objects
    """
    if not modeladmin.has_add_permission(request):
        raise PermissionDenied
    objs = clone_objects(queryset, request, ("name", "title"))
    messages.add_message(
        request,
        messages.SUCCESS,
        "Successfully duplicated %(count)d %(items)s." % {
            'count': len(queryset),
            'items': model_ngettext(modeladmin.opts, len(queryset))
        })


publish.short_description = "Publish selected %(verbose_name_plural)s"
unpublish.short_description = "Unpublish selected %(verbose_name_plural)s"
duplicate.short_description = "Duplicate selected %(verbose_name_plural)s"
make_staff.short_description = "Enable staff status for selected %(verbose_name_plural)s"
add_to_groups.short_description = "Add selected users to groups"
