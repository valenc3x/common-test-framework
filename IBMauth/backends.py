import ldap

from django.contrib.auth.models import User
from django.contrib.auth.backends import RemoteUserBackend


AUTH_LDAP_SERVER = 'bluepages.ibm.com'


class LDAPBackend(RemoteUserBackend):
    """
    Authentication via w3 Intranet (Bluepages LDAP)
    """
    create_unknown_user = True

    def authenticate(self, username=None, password=None):
        base = "ou=bluepages,o=ibm.com"
        scope = ldap.SCOPE_SUBTREE
        auth_filter = "(&(objectclass=person) (mail=%s))" % username
        ret = ['dn']
        user = None

        # Authenticate the base user so we can search
        try:
            ldap_conn = ldap.open(AUTH_LDAP_SERVER)
            ldap_conn.protocol_version = ldap.VERSION3
        except ldap.LDAPError:
            return None

        try:
            result_id = ldap_conn.search(base, scope, auth_filter, ret)
            result_type, result_data = ldap_conn.result(result_id, 0)

            # If the user does not exist in LDAP, Fail.
            if len(result_data) != 1:
                # Any way to show a meaningful error message for
                # auth.authenticate() method and admin.site.login()?
                return None

            # Attempt to bind to the user's DN
            ldap_conn.simple_bind_s(result_data[0][0], password)

        except (ldap.INVALID_CREDENTIALS, ldap.INAPPROPRIATE_AUTH):
            # Name or password were bad. Fail.
            # Any way to show a meaningful error message for
            # auth.authenticate() method and admin.site.login()?
            return None

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            try:
                user, created = User.objects.get_or_create(username=username)
                if created:
                    user = self.configure_user(user)
            except:
                # Might get rid of those IntegrityError messages (?)
                pass
        else:
            try:
                user = User.objects.get(username__exact=username)
            except User.DoesNotExist:
                pass

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def configure_user(self, user):
        """
        Configures a newly created user. This method is called immediately
        after a new user is created, and can be used to perform custom
        setup actions, such as setting the user's groups based on
        attributes in an LDAP directory. Returns the user object.
        """
        return user


class DummyBackend(RemoteUserBackend):
    """
    Bypass normal authentication schemes. Used for local development.
    """
    create_unknown_user = True

    def authenticate(self, username=None, password=None):
        user = None

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            try:
                user, created = User.objects.get_or_create(username=username)
                if created:
                    user = self.configure_user(user)
            except:
                # Might get rid of those IntegrityError messages?
                pass
        else:
            try:
                user = User.objects.get(username__exact=username)
            except User.DoesNotExist:
                pass

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def configure_user(self, user):
        """
        Configures a newly created user. This method is called immediately
        after a new user is created, and can be used to perform custom
        setup actions, such as setting the user's groups based on
        attributes in an LDAP directory. Returns the user object.
        """
        user.is_staff = 1
        user.is_active = 1
        user.is_superuser = 1
        user.save()
        return user
