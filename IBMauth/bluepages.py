import errno
import logging
import os
import re
import urllib
import urllib2

import ldap
import simplejson
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ImproperlyConfigured

# TODO: Use ImageMagik with PythonMagik instead
# from PIL import Image



# The query string to a bluepage profile
BLUEPAGES_QUERY_URL = getattr(settings, 'BLUEPAGES_QUERY_URL',
    "http://w3.ibm.com/bluepages/simpleSearch.wss?searchBy=Internet+address&location=All+locations&searchFor=%s")

# The query string for bluepages photos
BLUEPAGES_PHOTO_QUERY_URL = getattr(settings, 'BLUEPAGES_PHOTO_QUERY_URL',
    "http://w3.ibm.com/bluepages/api/BluePagesPhoto.jsp?EMAIL=%s")

# Image to use if there is no bluepage photo
BLUEPAGES_ANONYMOUS_PHOTO_URL = "http://w3.ibm.com/bluepages/photo/images/photo_none_small.gif"

# Amount of time to cache bluepages information
# 60 seconds * 60 minutes * 24 hours * 15 days
BLUEPAGE_CACHE_SECONDS = getattr(settings, 'BLUEPAGE_CACHE_SECONDS', 1296000)

# Directory to save bluepages photos
BLUEPAGES_CACHE_DIR = getattr(settings, 'BLUEPAGES_CACHE_DIR', os.sep.join([settings.MEDIA_ROOT, 'bluepages']))

# Web location of bluepages photos
BLUEPAGES_CACHE_URL = getattr(settings, 'BLUEPAGES_CACHE_URL', settings.MEDIA_URL + 'bluepages')

# Try to create the directory that will store bluepage photos
try:
    os.makedirs(BLUEPAGES_CACHE_DIR)
except OSError, e:
    if e.errno != errno.EEXIST:
        raise ImproperlyConfigured('MEDIA_ROOT directory is not writeable')


def get_user_info(username, ret=None):
    """
    Retrieve intranet user information from bluepages and cache the result
    Retrieving cached information currently does not work
    """
    # user = cache.get('bluepages_'+username) or {}
    # if user is None:
    AUTH_LDAP_SERVER = 'bluepages.ibm.com'
    base = "ou=bluepages,o=ibm.com"
    scope = ldap.SCOPE_SUBTREE
    filter = "(&(objectclass=person) (mail=%s))" % username
    if ret is None:
        ret = [
            'dn',
            'cn',
            'sn',
            'givenname',
            'preferredlastname',
            'jobresponsibilities',
            'notesemail'
        ]
    user = {
        'email': username,
        'name': '',
        'notesid': '',
        'title': '',
        'fullname': '',
        'lastname': '',
    }

    # Authenticate the base user so we can search
    try:
        l = ldap.open(AUTH_LDAP_SERVER)
        l.protocol_version = ldap.VERSION3
        result_id = l.search(base, scope, filter, ret)
        result_type, result_data = l.result(result_id, 0)
        # If the user does not exist in LDAP, Fail.
        if (len(result_data) != 1):
            return get_user_info_json(username)
        result = result_data[0][1]
    except:
        logging.error(username + ' not found in Bluepages (LDAP)')
        return get_user_info_json(username)  # Fall back to using JSON API

    user['notesid'] = get_notesid(result)
    user['title'] = get_title(result)
    user['fullname'] = get_fullname(result)
    user['firstname'] = user['fullname'][0]
    user['lastname'] = user['fullname'][1]
    user['name'] = user['fullname']

    # cache.set('bluepages_'+username, user)

    return user


def get_user_info_json(userid, params=None):
    """
    Retrieve intranet user information from bluepages and cache the result

    Retrieving cached information currently does not work
    """
    if params is None:
        params = [
            'cn',
            'sn',
            'givenname',
            'preferredlastname',
            'jobresponsibilities',
            'notesemail'
        ]
    param_str = ''
    for key in params:
        param_str += key + '&'

    # user = cache.get(userid + param_str) or {}
    user = {
        'email': userid,
        'name': '',
        'notesid': '',
        'title': '',
        'fullname': '',
        'lastname': '',
    }

    try:
        url = "http://bluepages.ibm.com/BpHttpApisv3/slaphapi?ibmperson/mail=%s.list/byjson?%s" % (userid.strip(), param_str)
        data = urllib2.urlopen(url).read()
        json = simplejson.loads(data)

        if json['search']['return']['count'] == 0:
            logging.error(userid + ' not found in Bluepages')
            return user

        # format the user object
        for obj in json['search']['entry'][0]['attribute']:
            user[obj['name']] = obj['value']

        user['notesid'] = get_notesid(user)
        user['title'] = get_title(user)
        user['fullname'] = get_fullname(user)
        user['firstname'] = user['fullname'][0]
        user['lastname'] = user['fullname'][1]
        user['name'] = user['fullname']

        # cache.set(userid+param_str, user, BLUEPAGE_CACHE_SECONDS)
    except KeyError:
        logging.error("Error parsing JSON result from Bluepages for '%s': %s" % (userid, str(user)))

    return  # user


def get_fullname(user):
    """
    Get an intranet user's full name (first name and last name)
    """
    ret = ('', '')

    if len(user) == 0:
        return ret

    try:
        firstname = user['givenname'][0]
        # remove middle initial
        if firstname[-1] is '.' and firstname[-3] is ' ':
            firstname = firstname[0:-3]
        lastname = user['sn'][0]
        return (firstname, lastname)
    except KeyError:
        try:
            name = user['notesemail'][0]
            name = name[name.find("=")+1:name.find("/")].split(" ", 1)
            return (name[0], name[1])
        except KeyError:
            pass

    return ret

def get_full_name(user):
    return get_fullname(user)


def get_notesid(user):
    """
    Get formatted notes id
    """
    try:
        regex = re.compile("CN=(.*)OU=(.*)O=(.*)")
        notesid = "".join(regex.search(user['notesemail'][0]).group(1, 2, 3))
    except KeyError:
        return ''

    return notesid


def get_title(user):
    """
    Get formatted job title
    """
    try:
        return user['jobresponsibilities'][0]
    except KeyError:
        return ''


def get_bluepage_url(intranet_id):
    """
    Shortcut function to get the URL to a user's bluepage profile
    """
    return BLUEPAGES_QUERY_URL % intranet_id


def get_bluepage_photo_url(intranet_id):
    """
    Returns a URL to the user's bluepage photo. Attempts to retrieve and
    cache the image locally and falls back to the original bluepages photo
    URL if that fails.
    """
    return BLUEPAGES_PHOTO_QUERY_URL % intranet_id


def clear_bluepage_photos():
    """ Clear bluepage photo directory """
    folder = BLUEPAGES_CACHE_DIR
    for file in os.listdir(folder):
        file_path = os.path.join(folder, file)
        try:
            os.unlink(file_path)
        except Exception, e:
            print e
