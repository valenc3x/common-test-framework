from django.conf import settings
from django.contrib.admin import StackedInline
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext, ugettext_lazy as _

import IBMadmin as admin

from IBMadmin.actions import make_staff, add_to_groups
from models import Contact

from forms import MyUserChangeForm, MyUserCreationForm


class MyContactInline(StackedInline):
    verbose_name = 'Other information'
    verbose_name_plural = 'Other information'
    model = Contact
    fk_name = 'user'
    extra = 1
    max_num = 1


class MyUserAdmin(UserAdmin):
    block_title = "Manage Users"
    actions = UserAdmin.actions
    actions = [make_staff]
    actions.append(add_to_groups)

    add_form_template = 'admin/auth/user/add_form.html'
    change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('username',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': (  # 'user_permissions'
            'is_active',
            'is_staff',
            'is_superuser')}),
        (_('Groups'), {'fields': ('groups',)}),
    )
    add_fieldsets = (
        (None, {
            'fields': ('username',),
        }),
    )
    form = MyUserChangeForm
    add_form = MyUserCreationForm

    list_display = (
        'username',
        'first_name',
        'last_name',
        'is_staff',
        'is_active'
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('username', 'first_name', 'last_name')
    ordering = ('username',)

    filter_horizontal = ()
    inlines = [MyContactInline]

    def get_queryset(self, request):
        """
        Remove ADMINS from user list
        """
        admins = list(x[1] for x in settings.ADMINS)
        queryset = super(MyUserAdmin, self).get_queryset(request)
        if request.user.username not in admins:
            queryset = queryset.exclude(username__in=admins)
        return queryset

    def has_delete_permission(self, request, obj=None):
        """
        Returns True if the given request has permission to change the given
        Django model instance.

        If `obj` is None, this should return True if the given request has
        permission to delete *any* object of the given type.
        """
        return False  # disable user deletion until delete bug can be fixed

    def get_actions(self, request):
        """
        Return a dictionary mapping the names of all actions for this
        ModelAdmin to a tuple of (callable, name, description) for each action.
        """
        actions = super(MyUserAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    class Media:
        js = [
            settings.MEDIA_URL + '/js/admin/MyUserAdmin.js',
        ]


class MyGroupAdmin(GroupAdmin):
    pass

admin.site.reregister(User, MyUserAdmin)
admin.site.register(Group)

