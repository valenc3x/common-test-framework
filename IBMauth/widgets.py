from django.forms.widgets import HiddenInput
from django.utils.safestring import mark_safe


class ReadOnly(HiddenInput):
    """
    Read-only widget that displays the value and stores it using a hidden input
    """
    def render(self, name, value, attrs=None):
        hidden = super(ReadOnly, self).render(name, value, attrs=attrs)
        return mark_safe(u"<p>%s</p>\n%s" % (value, hidden))
