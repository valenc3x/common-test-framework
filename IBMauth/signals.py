from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User

import bluepages

from models import Contact


def save_additional_user_info(sender, *args, **kwargs):
    """
    Run before a user is saved. Fetches additional
    user information from bluepages automatically
    """
    user = kwargs['instance']
    user.email = user.username
    user.password = ''
    if user.first_name is '' or user.last_name is '':
        try:
            bluepages_info = bluepages.get_user_info(user.username)
            user.first_name = bluepages_info['firstname']
            user.last_name = bluepages_info['lastname']
        except:
            user.first_name = ''
            user.last_name = ''


def save_additional_profile_info(sender, *args, **kwargs):
    """
    Run after a user is saved. Fetches additional
    user information from bluepages automatically
    """
    if kwargs['created']:
        user = kwargs['instance']
        try:
            bluepages_info = bluepages.get_user_info(user.username)
            title = bluepages_info['title']
        except:
            title = ''

        user_profile = Contact(user_id=user.id, title=title)

        try:
            # create Contact model which holds additional user info
            user_profile.save()
        except:
            # could catch IntegrityError but that would not be DB agnostic...
            pass

        bluepages.get_bluepage_photo_url(user.username)  # save bluepages image


pre_save.connect(save_additional_user_info, sender=User)
post_save.connect(save_additional_profile_info, sender=User)
