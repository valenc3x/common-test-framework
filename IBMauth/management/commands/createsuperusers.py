import sys
import datetime

from django.core.management.base import BaseCommand, NoArgsCommand, CommandError
from django.contrib.auth.models import User
from django.conf import settings
from django.db.utils import IntegrityError
from IBMauth.bluepages import get_user_info

class Command(BaseCommand):
    """
    BaseCommand.option_list = (
        make_option('-v', '--verbosity', action='store', dest='verbosity', default='1',
            type='choice', choices=['0', '1', '2'],
            help='Verbosity level; 0=minimal output, 1=normal output, 2=all output'),
        make_option('--settings',
            help='The Python path to a settings module, e.g. "myproject.settings.main". If this isn\'t provided, the DJANGO_SETTINGS_MODULE environment variable will be used.'),
        make_option('--pythonpath',
            help='A directory to add to the Python path, e.g. "/home/djangoprojects/myproject".'),
        make_option('--traceback', action='store_true',
            help='Print traceback on exception'),
    )
    """
    args = '<verbosity>'
    help = 'Automatically creates superusers based on ADMIN and MANAGERS settings'
    can_import_settings = True
    requires_model_validation = False
    
    def handle(self, *args, **options):
        verbose = 'test' not in sys.argv
        if verbose:
            print "Creating superusers..."

        users = settings.ADMINS

        for name, email in users:
            i = name.find(" ")
            user, c = User.objects.get_or_create(username=email)
            try:
                user_info = get_user_info(email)
            except:
                user_info = {
                    "firstname": name[:i],
                    "lastname": name[i+1:],
                }
            user.first_name = user_info["firstname"]
            user.last_name = user_info["lastname"]
            user.is_staff = True
            user.is_active = True
            user.is_superuser = True
            if c or not user.last_login:
                user.last_login = datetime.datetime.now()
            if c or not user.date_joined:
                user.date_joined = datetime.datetime.now()

            user.save() # TODO: this isn't firing off the post_save signals...

            if c and verbose:
                print "Created user %s..." % email
            elif verbose:
                print "Updated user %s..." % email
