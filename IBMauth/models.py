from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import connection, models, transaction
from django.utils.safestring import mark_safe

from . import bluepages


class NamedModelChoicesManager(models.Manager):
    """ Manager for a named model
    """

    def get_queryset(self):
        """ returns queryset for the Manager
        """
        return super(NamedModelChoicesManager, self).\
            get_queryset().values_list('id', 'name')


class NamedModel(models.Model):
    """
    A model with just a name
    """
    name = models.CharField(max_length=200)

    objects = models.Manager()
    choices = NamedModelChoicesManager()

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']
        abstract = True


class ContactManager(models.Manager):
    """
    This could potentially be confusing. Django has a paradigm
    called managers for managing querysets. The purpose of this
    particular [django] manager is to order a list of contacts
    by putting the demand program managers (ie. people manager)
    at the top of the list. Unfortunately, the word 'manager' is
    used in two totally different contexts in this code! The class
    name 'ContactManager' refers to the 'Contact' model, and the
    'Manager' in 'ContactManager' refers to the fact that it is of
    the type django.db.models.Manager
    """
    def get_queryset(self):
        """ for performance, always use select_related('user') for this model """
        return super(ContactManager, self).get_queryset().select_related('user')

    def ordered_by_manager(self):
        """ order contacts by demand program manager (ie. people manager) """

        def find_contact(contact, contacts):
            """ find index of given contact """
            for i, cont in enumerate(contacts):
                if cont.intranet().lower() == contact.lower():
                    return i
            return -1

        def by_last_name(person_a, person_b):
            """ compare contacts by last name """
            return cmp(person_a.user.last_name, person_b.user.last_name)

        # sort contacts by last name then put managers at the top
        contacts = sorted(self.get_query_set(), by_last_name)

        try:
            managers = [x[1] for x in getattr(settings, 'MANAGERS', [])]
            managers.reverse()
        except Exception:
            managers = []
        for manager in managers:
            i = find_contact(manager, contacts)
            if i != -1:
                contacts.insert(0, contacts.pop(i))

        return contacts


class Contact(models.Model):
    """
    Additional info associated with the 'User' model
    """
    user = models.ForeignKey(
        User,
        blank=True,
        unique=True,
        related_name="user"
    )
    title = models.CharField(
        max_length=255,
        verbose_name='Job title',
        null=True,
        blank=True,
        help_text="The contact's job title."
    )

    # managers
    objects = ContactManager()

    def intranet(self):
        return self.user.username

    def bluepage_url(self):
        return bluepages.get_bluepage_url(self.user.username)

    def bluepage_photo_url(self):
        return bluepages.get_bluepage_photo_url(self.user.username)

    def full_name(self):
        return self.user.first_name + ' ' + self.user.last_name

    @classmethod
    def cleanup(cls, *args, **kwargs):
        """ Delete invalid contacts (have no associated User) """
        cursor = connection.cursor()
        cursor.execute(
            "DELETE contact "
            "FROM %s contact LEFT JOIN %s user ON user.id = contact.user_id "
            "WHERE user.id IS NULL" %
            (cls()._meta.db_table, User()._meta.db_table)
        )
        transaction.commit_unless_managed()

    def __unicode__(self):
        if self.user.first_name:
            return self.user.first_name + ' ' + self.user.last_name
        else:
            return self.user.username

    class Meta:
        ordering = ['user__first_name', 'user__last_name']
        verbose_name = "Contact"


class ContactType(NamedModel):
    """
    This has nothing to do with the Contact model
    """
    class Meta(NamedModel.Meta):
        verbose_name = "Contact Type"
        managed = True


class JobFunctionManager(models.Manager):
    """ Manager for JobFunction model
    """
    def get_queryset(self):
        """ for performance, use select_related('contact_type') for this model
        """
        return super(JobFunctionManager, self).\
            get_queryset().select_related('contact_type')


class JobFunction(NamedModel):
    """
    Contact Type / Job Function (aka Role)
    """
    contact_type = models.ForeignKey('ContactType')

    # Override default manager
    objects = JobFunctionManager()

    def __unicode__(self):
        if self.name == "":
            return self.contact_type.name
        else:
            return "%s - %s" % (self.contact_type.name, self.name)

    class Meta:
        ordering = ['contact_type__name', 'name']
        verbose_name = "Job Function"
        managed = True


class JoinProjectRequest(models.Model):
    """ Model to manage join request for product teams
    """
    user = models.ForeignKey(User, related_name="requests")
    group = models.ForeignKey(Group, related_name="requests")
    approved = models.BooleanField(default=False, editable=False)

    def __unicode__(self):
        return "%s - %s " % (self.user.username, self.group.name)
