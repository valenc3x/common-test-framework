from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from . import __name__ as app_name


def get_templates(app, template=''):
    """
    Return a tuple of template directories given the name of a sub-app
    """
    base_app = getattr(settings, 'APPNAME', None)
    if base_app is None:
        raise ImproperlyConfigured("APPNAME setting is not defined")
    if template != "":
        template = "/" + template
    app = app.replace(base_app+'/', '').replace(app_name+'/', '')
    template_dir = "%s/%s%s" % (base_app, app, template)
    template_dir = "%s/%s%s" % (app_name, app, template)
    return [template_dir, template_dir]


def get_admin_templates(app, template=''):
    """ Adds an additional directory to search for admin templates """
    return get_templates(app, template) + ['admin/%s' % template]
