from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.forms import Field
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from .widgets import ReadOnly
from .bluepages import *
from .models import JoinProjectRequest


class ReadOnlyField(Field):
    """
    Django 'read_only' functionality does not work as expected
    """
    def __init__(self, *args, **kwargs):
        defaults = {
            'widget': ReadOnly,
            'required': False,
        }
        defaults.update(**kwargs)
        super(ReadOnlyField, self).__init__(*args, **defaults)


class IBMIntranetWidget(forms.widgets.TextInput):
    """
    Adds bluepages autocomplete to a text input
    """
    def render(self, name, value, attrs=None):
        attrs = attrs or {}
        auth_id = attrs['id']
        admin = settings.ADMINS[0][1]
        return super(IBMIntranetWidget, self).\
            render(name, value, attrs=attrs) + mark_safe(
                u'<script type="text/javascript">new BPACAutoComplete '
                u'("%s", { userEmail:"%s", '
                u'multiple:false});</script>' % (auth_id, admin))

    class Media:
        css = {
            'all': [
                settings.MEDIA_URL + '/css/bpac.css',
            ],
        }
        js = [
            settings.MEDIA_URL + '/js/yahoo-dom-event.js',
            settings.MEDIA_URL + '/js/autocomplete.js',
            settings.MEDIA_URL + '/js/bpac.js'
        ]


class IBMIntranetField(forms.RegexField):
    """
    Field to input an IBM Intranet address
    """
    widget = IBMIntranetWidget

    INTRANET_REGEX = r'^^[a-zA-Z0-9_\-\.\ ]+@([a-zA-Z0-9]+\.)+ibm\.com$'
    ERROR_MESSAGE = "The email address must be a valid IBM Intranet ID."

    def __init__(self, regex=None, error_message=None, *args, **kwargs):
        regex = regex or self.INTRANET_REGEX
        error_message = error_message or self.ERROR_MESSAGE
        super(IBMIntranetField, self).__init__(regex, error_message=error_message, *args, **kwargs)

    def pre_save(self, model_instance, add):
        string = getattr(model_instance, self.attname)
        # Remove leading and trailing whitespace
        string = string.strip()
        # save new value to model
        setattr(model_instance, self.attname, string)
        return super(IBMIntranetField, self).pre_save(model_instance, add)


class MyUserCreationForm(forms.ModelForm):
    """
    Custom form for creating new users
    """
    username = IBMIntranetField(
        label=_("Intranet ID"),
        max_length=100,
        help_text="Required. 100 characters or "
                  "fewer. Alphanumeric characters only"
                  " (letters, digits and underscores)."
    )

    def clean_username(self):
        username = self.cleaned_data['username']
        user_info = get_user_info(username)
        if "firstname" not in user_info or 'lastname' not in user_info:
            raise forms.ValidationError("Invalid intranet id %s" % username)
        return username

    def save(self, commit=True):
        user = super(MyUserCreationForm, self).save(commit=False)
        user.password = ''  # Do not save passwords
        user_info = get_user_info(user.username)
        user.first_name = user_info["firstname"]
        user.last_name = user_info["lastname"]
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ("username",)


class MyUserChangeForm(UserChangeForm):
    """
    Custom form for editing users
    """
    username = ReadOnlyField(label=_("Intranet ID"))

    def save(self, commit=True):
        user = super(MyUserChangeForm, self).save(commit=False)
        user.password = ''  # Do not save passwords
        if commit:
            user.save()
        return user

    def clean_password(self):
        pass
