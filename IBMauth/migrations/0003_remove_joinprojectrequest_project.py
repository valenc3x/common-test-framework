# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('IBMauth', '0002_joinprojectrequest_project'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='joinprojectrequest',
            name='project',
        ),
    ]
