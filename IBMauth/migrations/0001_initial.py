# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b"The contact's job title.", max_length=255, null=True, verbose_name=b'Job title', blank=True)),
                ('user', models.ForeignKey(related_name='user', blank=True, to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
                'ordering': ['user__first_name', 'user__last_name'],
                'verbose_name': 'Contact',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Contact Type',
                'abstract': False,
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobFunction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('contact_type', models.ForeignKey(to='IBMauth.ContactType')),
            ],
            options={
                'ordering': ['contact_type__name', 'name'],
                'verbose_name': 'Job Function',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JoinProjectRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('approved', models.BooleanField(default=False, editable=False)),
                ('group', models.ForeignKey(related_name='requests', to='auth.Group')),
                ('user', models.ForeignKey(related_name='requests', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
