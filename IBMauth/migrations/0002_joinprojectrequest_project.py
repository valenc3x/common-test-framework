# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0003_auto_20161201_1425'),
        ('IBMauth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='joinprojectrequest',
            name='project',
            field=models.ForeignKey(related_name='requests', default=1, to='CTAFramework.Product'),
            preserve_default=False,
        ),
    ]
