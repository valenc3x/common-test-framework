import re

from django import http
from django.conf import settings
from django.shortcuts import render_to_response


# Use try/catch in Middlewhere everywhere possible.
# Don't want middleware to bring down everything!


class BlockedUserAgentsAdminMiddleware(object):
    """
    Restrict certain user agents from the admin section
    """
    def process_request(self, request):
        try:
            if re.match(r'^/admin(/.*)?$', request.get_full_path()) \
                    is not None:
                for regex in settings.DISALLOWED_USER_AGENTS_ADMIN:
                    if regex.search(request.META['HTTP_USER_AGENT']) \
                            is not None:
                        return render_to_response('admin/browser.html')
        except:
            return None


class BlockedIpMiddleware(object):
    """
    Simple middlware to block IP addresses via settings variable DISALLOWED_IPS
    """
    def process_request(self, request):
        try:
            if request.META['REMOTE_ADDR'] in settings.DISALLOWED_IPS:
                return http.HttpResponseForbidden('<h1>Forbidden</h1>')
        except:
            return None
