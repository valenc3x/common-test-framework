function getOffset(el) {
    el = el.getBoundingClientRect();
    return {
        left: el.left + window.scrollX,
        top: el.top + window.scrollY
    }
}

var reset = function () {
    var container = $('#ibm-signin-minimenu-container');
    container.html('<li role="presentation"><a role="menuitem" href="/admin/login">Login</a></li>');
}

var stickyScroll = function () {

    $(window).scroll(function () {
        document.wrapperOffset = $('.code-zone').offset().top
        if ($(document).scrollTop() < document.wrapperOffset) {
            $('.code-wrapper')
                .removeClass('fixed-wrapper');
        } else {
            $('.code-wrapper')
                .addClass('fixed-wrapper');
        }
    });
}

$(function () {

    $('.ctf-masthead-categories').attr('class', 'ibm-masthead-categories' );
    $('.ibm-masthead-categories').css('display', 'block' );
    $('#ibm-signin-minimenu-container').html('');


    setTimeout(reset, 0);
    setTimeout(stickyScroll, 0);

});

