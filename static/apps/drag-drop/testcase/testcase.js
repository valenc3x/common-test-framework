var app = angular.module('demo');
app.requires.push('ngTagsInput');

app.controller("TestcaseController", function($scope, $http, $timeout, $httpParamSerializerJQLike) {

  $http.get('/api/advanced').then(function (response) {
    $scope.advanced_options = response.data;
  });

  $scope.initialLoad = true;

  $scope.show = '';
  $scope.tem = false;
  $scope.preview = '';
  $scope.libraries = [];
  $scope.run_testcase = {
    host: '',
    user: '',
    passw: ''
  }
  $scope.instance = {
    repository_id: undefined,
    name: '',
    tags: '',
    product_version: '',
    robot_data: '',
    product_id: undefined,
    help_text: '',
    author_id: undefined,
    json_data: '',
    id: undefined,
    phase_id: undefined
  }
  $scope.tags = [];
  $scope.dropdown = {};
  $scope.models = {
    "selected": null,
    "templates": [
      {"type": "item", "id": 2},
      {"type": "variable", "id": 4, "columns":[]},
      {"type": "coments", "id": 5},
      {"type": "python", "id": 7},
      {"type": "tags", "id": 6},
      {"type": "container", "id": 1, "columns":[[], []]},
      {"type": "elcontainer", "id": 3, "columns":[]},
      {"type": "rvariable", "id": 4, "columns":[]},
    ],
    "dropzones": {
      "Keyword": [ ],
      "Control Flow Sentences": [
        {
          "type": "variable",
          "info":{"key": "vt","name": "$variable"},
          "values": [{ "command": "variable", "value": " ", "comment": " ", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "variable",
          "info":{"key": "vl","name": "@list"},
          "values": [{ "command": "list", "value": "", "comment": "", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "coments",
          "info":{"key":"cn","name": "$comment"},
          "values": [{ "command": "Comment in line", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "python",
          "info":{"key":"cm","name": "$python"},
          "values": [{ "command": "Command", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "tags",
          "info":{"key":"tg","name": "[TAG]"},
          "values": [{ "command": "Tag", "comentario": "", "value": "", "tags":[] }]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fi","name": "for"},
          "values":[{"condition1": "",  "condition2": "in","condition3": "",}],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fr","name": "for"},
          "values":[{"condition1": "", "condition2": "in range", "condition3": ""}],
          "innerdrop": [ [ ] ]
        }
      ],
      "Robot command": [
        {
          "type": "python",
          "info":{"key":"st","name": "$rsetting"},
          "values":[{ "command":"setting", "comment":"Agregar testcasea de comentario", "value":"------", "required":true}]
        },
        {
          "type": "rvariable",
          "info":{"key":"sc","name": "@rlist"},
          "values":[{ "command":"title", "value":"", "comment":"Comentarios de la a a la z", "required":true}],
          "advanced":[{"user":"_____","password":"_____","ssh-key":"_____","mandatory":"_____","expectToFail":"_____","background":"_____","useSudo":"_____","failMessage":"_____","successMessage":"_____","target":"_____"}],
          "columns": [
            [
            ]
          ]
        },
        {
          "type": "rvariable",
          "info":{"key":"sk","name": "@rkeyword"},
          "values":[{ "command":"keyword's", "value":"", "comment":"Comentarios de la a a la z", "required":true}],
          "advanced":[{"user":"_____","password":"_____","ssh-key":"_____","mandatory":"_____","expectToFail":"_____","background":"_____","useSudo":"_____","failMessage":"_____","successMessage":"_____","target":"_____"}],
          "columns": [
            [
            ]
          ]
        }
      ],
      "System": [ ],
      "Product": [ ],
      "RobotFramework": [ ],
      "CustomKeyword": [ ],
      "Resource external": [ {
        "type": "item",
        "info":{"key":"ck","name": "Custom Keyword"},
        "values":[{ "command":"Custom","comment":"That keyword that you know your USE ", "value":"name and use external keyword", "required":true,"input":"text"}],
        "advanced":[{"user":"_____","password":"_____","ssh-key":"_____","mandatory":"_____","expectToFail":"_____","background":"_____","useSudo":"_____","failMessage":"_____","successMessage":"_____","target":"_____"}]
      }]
    }
  };

  $http.get('/api/products').then(function (response) {
    $scope.dropdown.products = response.data;
  });

  $scope.update_items = function (obj_id) {

    // dropdown items
    $http.get('/api/repositories/' + obj_id).then(function (response) {
      $scope.dropdown.repositories = response.data;
      if ($scope.initialLoad){
        setTimeout(loadRepository, 0);
      }
    });

    $http.get('/api/phases/' + obj_id).then(function (response) {
      $scope.dropdown.phases = response.data;
      if ($scope.initialLoad) {
        setTimeout(loadPhase, 0);
      }
    });
    // Dropzone items
    $http.get('/api/commands/system/' + obj_id).then(function (response) {

      $scope.models.dropzones.System = response.data;
    });

    $http.get('/api/commands/product/' + obj_id).then(function (response) {
      $scope.models.dropzones.Product = response.data;
    });

    $http.get('/api/keywords/framework/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.RobotFramework = result;
    });

    $http.get('/api/keywords/custom/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.CustomKeyword = result;
    });
  };

  // Watch
  $scope.$watch('models.dropzones', function(model) {
    $scope.modelAsJson = angular.toJson(model, true);
  }, true);

  var loadProduct = function () {
    $('select[name=product] option[value=' + $scope.instance.product_id + ']').attr('selected', true);
    setTimeout($scope.update_items, 0, $scope.instance.product_id);
  }

  var loadRepository = function () {
    $('select[name=repository] option[value=' + $scope.instance.repository_id + ']').attr('selected', true);
  }

  var loadPhase = function () {
    $('select[name=phase] option[value=' + $scope.instance.phase_id + ']').attr('selected', true);
    $scope.initialLoad = false;
  }

  var getInstance = function () {
    // Edit Mode
      var instance_url = '/api/instance/' + document.edit_data.model + '/' + document.edit_data.object_id;
      $http.get(instance_url).then(function(response){
        $scope.instance = response.data;
        $scope.instance.product_id = "" + $scope.instance.product_id;
        $scope.instance['fromversion'] = $scope.instance.product_version.split(',')[0];
        $scope.instance['toversion'] = $scope.instance.product_version.split(',')[1];
        angular.forEach( $scope.instance.tags.split(','), function (item, key) {
          $scope.tags[key] = { 'text': item };
        });
        $scope.models.dropzones.Keyword = JSON.parse($scope.instance.json_data);
        $scope.models.dropzones.Keyword.shift();
        setTimeout(loadProduct, 0);
      });
  }

  if (document.edit_mode) {
    setTimeout(getInstance, 0);
  }

  $scope.sedarray=function(){
    var xd1=$scope.models.dropzones.Keyword;
    var fullArray = [];
    var temporalshow = [];
    angular.forEach(xd1, function(a, key) {
      fullArray.push(a);
      angular.forEach(a, function(b, c) {
        if(c === "id"){
          temporalshow.push(b);
        }
        if (c !== "type" && c !== "id" && c !== "$$hashKey"){
          angular.forEach(b, function(x , v) {
            if (x.valor !== "") {
              if(x.valor === true){
                temporalshow.push(x.comando);
              }
              if (x.requerido === true) {
                temporalshow.push(x.comando + " " + x.valor);
              }
            }
          });
        }
      });
    });

    if ($scope.models.dropzones.Keyword.length>0) {
      if ($scope.tem) {
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }else{
        $scope.tem = true;
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }
    }else{
      alert("Drop Zone is Empty");
      return;
    }
    $scope.show = xd1;
    $scope.code2 = xd1;
    $scope.instance.json_data = $scope.show;
    $scope.preview_robot();

  };

  $scope.closemodal=function(){
    if ($scope.models.dropzones.Keyword.length>0) {
        $scope.models.dropzones.Keyword.shift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
    }else{
        alert("Drop Zone is Empty");
        return;
    }
  };

  $scope.save_data = function(save_as=false, run=false){
    if (save_as){
      $('input[name=id]').remove();
      $scope.instance.id = null;
    }

    var form = $('form#testcase-form');
    var csrf = $('input[name=csrfmiddlewaretoken]').val();
    // product version
    var p_version = [$('input#fromversion').val() || '',  $('input#toversion').val() || ''];
    $('input[name=product_version]').attr('value', p_version.join(','));
    // tags
    var tagArray = [];
    angular.forEach($scope.tags, function(item, key){
      tagArray[key] = item.text;
    });
    var tags = tagArray.join(',');
    $('input[name=tags]').attr('value', tags.replace('-', ' '));
    form.append('<input type="hidden" name="csrfmiddlewaretoken" value="' + csrf + '">');
    if (run){
      form.append('<input type="hidden" name="run"      value="true">');
      form.append('<input type="hidden" name="host"     value="'+ $scope.run_testcase.host +'">');
      form.append('<input type="hidden" name="user" value="'+ $scope.run_testcase.user +'">');
      form.append('<input type="hidden" name="passw"    value="'+ $scope.run_testcase.passw +'">');
      form.append('<input type="hidden" name="used_libraries" value="' + $scope.libraries.toString() + '">');
    }
    form.submit();
  };

  $scope.preview_robot = function () {
    console.log('preview robot');
    var ins = $scope.instance.json_data;
    var json_data = JSON.stringify(ins);
    var libs = [];
    ins.slice(1).forEach(function(d,i){if(d.info) libs.push(d.info.library_id);});
    $scope.libraries = libs;
    $http({
      method: 'POST',
      url: '/api/preview/2',
      data: $httpParamSerializerJQLike({
        "json_data": json_data
      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function (response) {
      $scope.instance.robot_data = response.data.text;
      $scope.preview = response.data.text;
    });
  };

}); // angular controller
