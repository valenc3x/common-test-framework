
var app = angular.module('demo');
app.requires.push('ngTagsInput');

app.controller("KeywordController", function ($scope, $http, $location, $httpParamSerializerJQLike) {

  $scope.initialLoad = true;

  $scope.keyword_args = [];

  $http.get('/api/advanced').then(function (response) {
    $scope.advanced_options = response.data;
  });

  $scope.instance = {
    id: undefined,
    name: '',
    help_text: '',
    product_id: undefined,
    library_id: undefined,
    json_data: [],
    robot_data: ''
  };

  $scope.nameMain = {namekeyword: '', descriptionkeyword: ''};
  $scope.show = '';
  $scope.tem = false;
  $scope.preview = '';
  $scope.tags = [];
  $scope.dropdown = {};


  $scope.models = {
    "selected": null,
    "templates": [
      {"type": "item", "id": 2},
      {"type": "variable", "id": 4, "innerdrop":[]},
      {"type": "coments", "id": 5},
      {"type": "python", "id": 7},
      {"type": "tags", "id": 6},
      {"type": "container", "id": 1, "innerdrop":[[], []]},
      {"type": "elcontainer", "id": 3, "innerdrop":[]},
      {"type": "keyadd", "id": 8, "innerdrop":[]}
    ],
    "dropzones": {
      "Keyword": $scope.instance.json_data,
      "Control Flow Sentences": [
        {
          "type": "variable",
          "info":{"key": "vt","name": "$variable"},
          "values": [{ "command": "variable", "value": " ", "comment": " ", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "variable",
          "info":{"key": "vl","name": "@list"},
          "values": [{ "command": "list", "value": "", "comment": "", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "coments",
          "info":{"key":"cn","name": "$comment"},
          "values": [{ "command": "Comment in line", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "python",
          "info":{"key":"cm","name": "$python"},
          "values": [{ "command": "Command", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "tags",
          "info":{"key":"tg","name": "[TAG]"},
          "values": [{ "command": "Tag", "comentario": "", "value": "", "tags":[] }]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fi","name": "for"},
          "values":[{"condition1": "",  "condition2": "in","condition3": "",}],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fr","name": "for"},
          "values":[{"condition1": "", "condition2": "in range", "condition3": ""}],
          "innerdrop": [ [ ] ]
        }
      ],
      "System": [],
      "Product": [],
      "RobotFramework": [],
      "CustomKeyword": [],
      "Resource external": [
        {
          "type": "keyadd",
          "info":{"key": "ck","name": "Custom Keyword"},
          "values":[{ "command":"Custom","comment":"That keyword that you know your USE ", "value":"name and use external keyword", "required":true,"input":"text"}],
          "advanced":$scope.advanced_options,
          "innerdrop": [ [ ] ]
        },
        {
          "type": "keyadd",
          "info":{"key": "ck","name": "Custom Keyword"},
          "values":[{ "command":"Custom","comment":"That keyword that you know your USE ", "value":"name and use external keyword", "required":true,"input":"text"}],
          "advanced":$scope.advanced_options
        }
      ]
    }
  };


  $http.get('/api/products').then(function (response) {
    $scope.dropdown.products = response.data;
  });


  // Drag and Drop items
  $scope.update_items = function (obj_id) {

    $http.get('/api/libraries/' + obj_id).then(function (response) {
      $scope.dropdown.libraries = response.data;
      if ($scope.initialLoad) {
        setTimeout(loadLibrary, 0);
        $scope.initialLoad = false;
      }
    });

    $http.get('/api/keywords/framework/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.RobotFramework = result;
    });

    $http.get('/api/keywords/custom/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.CustomKeyword = result;
    });

    $http.get('/api/commands/system/' + obj_id).then(function(response) {
      $scope.models.dropzones.System = response.data;
    });

    $http.get('/api/commands/product/' + obj_id).then(function(response) {
      $scope.models.dropzones.Product = response.data;
    });
  };


  // Edit Mode
  var loadProduct = function () {
    $('select[name=product] option[value=' + $scope.instance.product_id + ']').attr('selected', true);
    setTimeout($scope.update_items, 0, $scope.instance.product_id);
  }


  var loadLibrary = function () {
    $('select[name=library] option[value=' + $scope.instance.library_id + ']').attr('selected', true);
  }


  var getInstanceData = function () {
    var instance_url = '/api/instance/' + document.edit_data.model + '/' + document.edit_data.object_id;
    $http.get(instance_url).then(function(response){
      $scope.instance = response.data;
      $scope.product_id = response.data.product_id;
      $scope.models.dropzones.Keyword = JSON.parse($scope.instance.json_data);
      $scope.models.dropzones.Keyword.shift();
      setTimeout(loadProduct, 0);
    });
  }

  if (document.edit_mode) {
    setTimeout(getInstanceData, 0);
  }


  $scope.$watch('models.dropzones', function(model) {
    $scope.modelAsJson = angular.toJson(model, true);
  }, true);


  $scope.sedarray = function(){
    var xd1 = $scope.models.dropzones.Keyword;
    var fullArray = [];
    var temporalshow = [];

    angular.forEach(xd1, function(a, key) {
      fullArray.push(a);
      angular.forEach(a, function(b, c) {
        if (c === "id") {
          temporalshow.push(b);
        }
        if (c !== "type" && c !== "id" && c !== "$$hashKey"){
          angular.forEach(b, function(x , v) {
            if (x.valor !== "") {
              if(x.valor === true){
                temporalshow.push(x.comando);
              }
              if (x.required === true) {
                temporalshow.push(x.comando + " " + x.valor);
              }
            }
          });
        }
      });
    });

    if ($scope.models.dropzones.Keyword.length>0) {
      if ($scope.tem) {
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }else{
        $scope.tem = true;
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }
    }else{
      alert("Drop Zone is Empty");
      return;
    }
    $scope.show = xd1;
    $scope.code2 = xd1;
    $scope.instance.json_data = $scope.show;
    $scope.preview_robot();

  };


  $scope.closemodal = function(){
    if ($scope.models.dropzones.Keyword.length>0) {
      $scope.models.dropzones.Keyword.shift({
        namekeyword: $scope.instance.name,
        descriptionkeyword: $scope.instance.help_text
      });
    }else{
      alert("Drop Zone is Empty");
      return;
    }

  };


  $scope.save_data = function (save_as=false) {
    if (save_as){
      $('input[name=id]').remove();
      $scope.instance.id = 0;
    }
    var form = $('form#save-keyword-form');
    var csrf = $('input[name=csrfmiddlewaretoken]').val();
    form.append('<input type="hidden" name="csrfmiddlewaretoken" value="'+ csrf +'">');
    form.append('<input type="hidden" name="customarguments" value="'+ $scope.keyword_args.join() +'">');
    form.submit();
  };


  $scope.preview_robot = function () {
    var json_data = JSON.stringify($scope.instance.json_data);
    $http({
      method: 'POST',
      url: '/api/preview/1',
      data: $httpParamSerializerJQLike({
        "json_data": json_data
      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function (response) {
      $scope.instance.robot_data = response.data.text;
      $scope.preview = response.data.text;
      $scope.keyword_args = response.data.args;
    });
  };


});
