
angular.module("demo", ["ngRoute", "dndLists"])
  .config(function ($routeProvider, $locationProvider) {
    if(document.edit_mode){
      if(document.edit_data.model==="CustomKeyword"){
        $routeProvider
          .when('/customkeyword/add/', {
            templateUrl: window.BASE_URL + 'keyword/keyword-frame.html',
            controller: 'KeywordController'
          })
          .otherwise({
            templateUrl: window.BASE_URL + 'keyword/keyword-frame.html',
            controller: 'KeywordController'
          });
      }
      if(document.edit_data.model==="TestCase"){
        $routeProvider
          .when('/testcase/add/', {
            templateUrl: window.BASE_URL + 'testcase/testcase-frame.html',
            controller: 'TestcaseController'
          })
          .otherwise({
            templateUrl: window.BASE_URL + 'testcase/testcase-frame.html',
            controller: 'TestcaseController'
          });
      }
      if(document.edit_data.model==="TestSuite"){
        $routeProvider
          .when('/testsuite/add/', {
            templateUrl: window.BASE_URL + 'testsuite/testsuite-frame.html',
            controller: 'TestsuiteController'
          })
          .otherwise({
            templateUrl: window.BASE_URL + 'testsuite/testsuite-frame.html',
            controller: 'TestsuiteController'
          });
      }
    }else{
      $routeProvider
        .when('/customkeyword/add/', {
          templateUrl: window.BASE_URL + 'keyword/keyword-frame.html',
          controller: 'KeywordController'
        })
        .when('/testcase/add/', {
          templateUrl: window.BASE_URL + 'testcase/testcase-frame.html',
          controller: 'TestcaseController'
        })
        .when('/testsuite/add/', {
          templateUrl: window.BASE_URL + 'testsuite/testsuite-frame.html',
          controller: 'TestsuiteController'
        });
    }

      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
      });
  })

  .directive('navigation', function($rootScope, $location) {
    return {
      template:   '<li ng-repeat="option in options" ng-class="{active: isActive(option)}">' +
            '    <a ng-href="{{option.href}}">{{option.label}}</a>' +
            '</li>',
      link: function (scope, element, attr) {
        scope.options = [
          {label: "testcase", href: "/testcase"},
          {label: "Rutine function", href: "/customkeyword"},
        ];

        scope.isActive = function(option) {
          return option.href.indexOf(scope.location) === 1;
        };

        $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
          if (document.edit_mode) {
            scope.location = $location.path() + '/' + document.edit_data['object_id'];
          } else {
            scope.location = $location.path() + '/add';
          }
        });
      }
    };
  });
