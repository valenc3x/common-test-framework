var app = angular.module('demo');
app.requires.push('ngTagsInput');


app.controller("TestsuiteController", function ($scope, $http, $httpParamSerializerJQLike) {

  $http.get('/api/advanced').then(function (response) {
    $scope.advanced_options = response.data;
  });

  $scope.tags = [];
  $scope.dropdown = {};
  $scope.tem = false;

  $scope.instance = {
    help_text: "",
    id: null,
    json_data: "",
    name: "",
    product_id: null,
    product_version: "",
    robot_data: ""
  };

  $scope.models = {
    "selected": null,
    "templates": [
      {"type": "item", "id": 2},
      {"type": "variable", "id": 4, "columns":[]},
      {"type": "coments", "id": 5},
      {"type": "python", "id": 7},
      {"type": "tags", "id": 6},
      {"type": "container", "id": 1, "columns":[[], []]},
      {"type": "elcontainer", "id": 3, "columns":[]},
      {"type": "rvariable", "id": 8, "columns":[]},
      {"type": "suiteItem", "id": 9},
    ],
    "dropzones": {
      "Keyword": [ ],
      "Control Flow Sentences": [
        {
          "type": "variable",
          "info":{"key": "vt","name": "$variable"},
          "values": [{ "command": "variable", "value": " ", "comment": " ", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "variable",
          "info":{"key": "vl","name": "@list"},
          "values": [{ "command": "list", "value": "", "comment": "", "required": true }],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "coments",
          "info":{"key":"cn","name": "$comment"},
          "values": [{ "command": "Comment in line", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "python",
          "info":{"key":"cm","name": "$python"},
          "values": [{ "command": "Command", "comment": "", "value": "", "required": true }]
        },
        {
          "type": "tags",
          "info":{"key":"tg","name": "[TAG]"},
          "values": [{ "command": "Tag", "comentario": "", "value": "", "tags":[] }]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fi","name": "for"},
          "values":[{"condition1": "",  "condition2": "in","condition3": "",}],
          "innerdrop": [ [ ] ]
        },
        {
          "type": "elcontainer",
          "info":{"key":"fr","name": "for"},
          "values":[{"condition1": "", "condition2": "in range", "condition3": ""}],
          "innerdrop": [ [ ] ]
        }
      ],
      "Robot command": [
        {
          "type": "python",
          "info":{"key":"st","name": "$rsetting"},
          "values":[{ "command":"setting", "comment":"Agregar testcasea de comment", "value":"------", "required":true}]
        },
        {
          "type": "rvariable",
          "info":{"key":"sc","name": "@rlist"},
          "values":[{ "command":"title", "value":"", "comment":"comments de la a a la z", "required":true}],
          "columns": [ [ ] ]
        },
        {
          "type": "rvariable",
          "info":{"key":"sk","name": "@rkeyword"},
          "values":[{ "command":"keyword's", "value":"", "comment":"comments de la a a la z", "required":true}],
          "columns": [ [ ] ]
        }
      ],
      "System": [ ],
      "Product": [ ],
      "RobotFramework": [ ],
      "CustomKeyword": [ ],
      "Resource external": [ {
        "type": "item",
        "info":{"key":"ck","name": "Custom Keyword"},
        "values":[{ "command":"Custom","comment":"That keyword that you know your USE ", "value":"name and use external keyword", "required":true,"input":"text"}],
        "advanced":$scope.advanced_options
      } ],
      "TestCase": [ ]
    }
  };

  // Dropdowns
  $http.get('/api/products').then(function (response) {
    $scope.dropdown.products = response.data;
  });

  $scope.update_items = function (obj_id) {
    // Dropzone items
    $http.get('/api/commands/system/' + obj_id).then(function (response) {
      $scope.models.dropzones.System = response.data;
    });

    $http.get('/api/commands/product/' + obj_id).then(function (response) {
      $scope.models.dropzones.Product = response.data;
    });

    $http.get('/api/testcases/' + obj_id).then(function (response) {
      $scope.models.dropzones.TestCase = response.data;
    });

    $http.get('/api/keywords/framework/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.RobotFramework = result;
    });

    $http.get('/api/keywords/custom/' + obj_id).then(function (response) {
      var result = response.data;
      angular.forEach(result, function (item, index) {
        item['advanced'] = $scope.advanced_options;
      });
      $scope.models.dropzones.CustomKeyword = result;
    });
  };

  $scope.$watch('models.dropzones', function(model) {
    $scope.modelAsJson = angular.toJson(model, true);
  }, true);

  // Edit Mode

  var loadFields = function () {
    $('select[name=product] option[value=' + $scope.instance.product_id + ']').attr('selected', true);
    $scope.update_items($scope.instance.product_id);
  }


  if(document.edit_mode){
    var instance_url = '/api/instance/' + document.edit_data.model + '/' + document.edit_data.object_id;
    $http.get(instance_url).then(function(response){
      $scope.instance = response.data;
      document.instance = response.data;
      $scope.instance['fromversion'] = $scope.instance.product_version.split(',')[0];
      $scope.instance['toversion'] = $scope.instance.product_version.split(',')[1];
      if ($scope.instance.json_data === '') {
        $scope.instance.json_data = "[]";
      }
      $scope.models.dropzones.Keyword = JSON.parse($scope.instance.json_data);
      $scope.models.dropzones.Keyword.shift();
      setTimeout(loadFields, 0);
    });
  }

  $scope.sedarray=function(){
    var xd1 = $scope.models.dropzones.Keyword;
    var fullArray = [];
    var temporalshow = [];
    angular.forEach(xd1, function(a, key) {
      fullArray.push(a);
      angular.forEach(a, function(b, c) {
        if (c === "id") {
          temporalshow.push(b);
        }
        if (c!=="type" && c!=="id" && c!=="$$hashKey"){
          angular.forEach(b, function(x , v) {
            if (x.value !== "") {
              if(x.value === true){
                temporalshow.push(x.command);
              }
              if (x.required === true) {
                temporalshow.push(x.command + " " + x.value);
              }
            }
          });
        }
      });
    });

    if ($scope.models.dropzones.Keyword.length>0) {
      if ($scope.tem) {
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }else{
        $scope.tem = true;
        xd1.unshift({
          namekeyword: $scope.instance.name,
          descriptionkeyword: $scope.instance.help_text
        });
      }
    }else{
      alert("Drop Zone is Empty");
      return;
    }
    $scope.show = xd1;
    $scope.code2 = xd1;
    $scope.instance.json_data = $scope.show;
    $scope.preview_robot();
  };

  $scope.closemodal=function(){
    if ($scope.models.dropzones.Keyword.length>0) {
      $scope.models.dropzones.Keyword.shift({
        namekeyword: $scope.instance.name,
        descriptionkeyword: $scope.instance.help_text
      });
    }else{
      alert("Dropzone is empty");
      return;
    }
  };

  $scope.save_data = function(save_as=false){
    var form = $('form#testsuite-form');
    var csrf = $('input[name=csrfmiddlewaretoken]').val();

    if (save_as) {
      $('input[name=id]').remove();
      $scope.instance.id = null;
    }

    // product version
    var p_version = [$('input#fromversion').val() || '',  $('input#toversion').val() || ''];
    $('input[name=product_version]').attr('value', p_version.join(','));

    form.append('<input type="hidden" name="csrfmiddlewaretoken" value="'+ csrf +'">');
    form.submit();
  };

  $scope.preview_robot = function () {
    var json_data = JSON.stringify($scope.instance.json_data);
    console.log(json_data);
    $http({
      method: 'POST',
      url: '/api/preview/3',
      data: $httpParamSerializerJQLike({
        "json_data": json_data
      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function (response) {
      $scope.preview = response.data.text;
    });

  };

});
