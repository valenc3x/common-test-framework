from django.shortcuts import render
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import *


# TODO:
# Keywords
# admin.site.register(FrameworkLibrary)
# admin.site.register(CustomLibrary)
# admin.site.register(FrameworkKeyword)
# admin.site.register(CustomKeyword)
# admin.site.register(KeywordOption)

# # Test Cases
# admin.site.register(TestSuite)
# admin.site.register(TestCase)


# CLASS BASED VIEWS
class CommandOptionList(ListView):
    model = CommandOption
    context_object_name = 'command_option_list'
    template_name = 'commandoption_list.html'


class CommandOptionDetailView(DetailView):
    queryset = CommandOption.objects.all()

    def get_object(self):
        object = super(CommandOptionDetailView, self).get_object()
        object.last_accessed = timezone.now()
        object.save()
        return object


class CommandRuleList(ListView):
    model = CommandRule


class CommandRuleDetailView(DetailView):
    queryset = CommandRule.objects.all()

    def get_object(self):
        object = super(CommandRuleDetailView, self).get_object()
        object.last_accessed = timezone.now()
        object.save()
        return object


class TestCaseList(ListView):
    model = TestCase
    context_object_name = 'test_case_list'
    template_name = 'testcase_list.html'


class TestSuiteList(ListView):
    model = TestSuite
    context_object_name = 'test_suite_list'
    template_name = 'testsuite_list.html'


class CommandOptionCreate(CreateView):
    model = CommandOption
    fields = [
        'command', 'option', 'help_text', 'prefix', 'suffix', 'expected',
        'mandatory'
    ]
    template_name = 'commandoption_form.html'


class CommandOptionUpdate(UpdateView):
    model = CommandOption
    fields = [
        'command', 'option', 'help_text', 'prefix', 'suffix', 'expected',
        'mandatory', 'dependent_of', 'incompatibility', 'position'
    ]
    template_name_suffix = '_update_form'
    template_name = 'commandoption_update_form.html'


# class CommandOptionDelete(DeleteView):
#     model = CommandOption
#     success_url = reverse_lazy('commandoption-list')
