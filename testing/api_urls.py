from django.conf.urls import patterns, url

from .api import *

urlpatterns = patterns(
    '',
    url(r'^repositories/(?P<p_id>\d+)$', get_repositories, name="get_repositories"),
    url(r'^phases/(?P<p_id>\d+)$', get_phases, name="get_phases"),
    url(r'^products$', get_user_products, name="get_user_products"),
    url(r'^advanced$', get_advanced_options, name="get_advanced_options"),
    url(r'^libraries/(?P<p_id>\d+)$', get_libraries, name="get_libraries"),
    url(r'^testcases/(?P<p_id>\d+)$', get_testcases, name="get_testcases"),
    url(r'^commands/system/(?P<p_id>\d+)$', get_system_commands, name="get_system_commands"),
    url(r'^commands/product/(?P<p_id>\d+)$', get_product_commands, name="get_product_commands"),
    url(r'^keywords/custom/(?P<p_id>\d+)$', get_custom_keywords, name="get_custom_keywords"),
    url(r'^keywords/framework/(?P<p_id>\d+)$', get_framework_keywords, name="get_framework_keywords"),
    url(r'^instance/(?P<model>\w+)/(?P<obj_id>\d+)$', get_instance_data, name="get_instance_data"),

    url(r'^preview/(?P<mode>\d+)$', get_preview, name="get_preview"),
)
