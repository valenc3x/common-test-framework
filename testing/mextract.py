"""M Extract class module"""
import os
import re
from types import MethodType

import pexpect


from .models import Command, CommandOption

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
MEDIA_ROOT = BASE_DIR + '/media/'


try:
    from pexpect import pxssh
except Exception, e:
    def spawn(self):
        """"Not supported function error"""
        print 'Not supported'
    pexpect.spawn = MethodType(spawn, pexpect, pexpect)


class MExtract:
    """Class to manage parsing man pages
    """

    def __init__(self):
        self.script = 'getCommands.sh'
        self.check_sections = (
            'NAME',
            'SYNOPSIS',
            'DESCRIPTION',
            'OPTIONS',
            'SEE-ALSO'
        )

    def set_ssh_info(self, user, host, password, port="22"):
        self.user = user
        self.host = host
        self.pssw = password
        self.port = port if port else "22"

    def get_ssh_info(self):
        return "%s:%s@%s:%s" % (self.user, self.pssw, self.host, self.port)

    def _create_manfile_on_target(self, man_list):
        print "man on target"
        ssh = pxssh.pxssh()
        ssh.login(self.host, self.user, self.pssw, port=self.port)
        ssh.prompt()
        ssh.sendline("echo man_$(date +\%a-\%d-\%b-\%H.\%M.\%S).txt")
        ssh.prompt()
        man_file = ssh.before.split('\r\n')[1]
        ssh.sendline('touch ' + man_file)
        ssh.prompt()
        print man_file
        for command in man_list:
            ssh.sendline('man %s | col -b >> %s' % (command, man_file))
            ssh.prompt()
            ssh.sendline('echo END_MAN >> %s' % man_file)
            ssh.prompt()
        ssh.logout()
        return man_file

    def _get_manfile_from_target(self, man_file):
        scp_path = 'scp -P {0} {1}@{2}:~/{3} {4}m-extract_results/{3}'.format(
            self.port,      # 0
            self.user,      # 1
            self.host,      # 2
            man_file,       # 3
            MEDIA_ROOT)     # 4
        print scp_path
        system = pexpect.spawn(scp_path)
        system.expect('password:')
        system.sendline(self.pssw)
        system.expect('100%', timeout=600)
        all_file = ""
        with open('{0}m-extract_results/{1}'.format(MEDIA_ROOT, man_file)) as m_file:
            all_file = m_file.read()
        return all_file

    def _man_as_dict(self, man_list):
        """Parse a line-split manpage and separate into sections

        Arguments:
            man_list {list} -- a manpage information split into lines
        Returns:
            [dict] -- a dictionary with sections as top-level keys.
                        DESCRIPTION section is parsed to separate arguments and
                        their description
        """
        sections_dict = dict()
        section_name = ""
        indent = 0
        option = ''
        position = -1
        # Skip header and footer for manpage
        for line in man_list[1:-2:]:
            tmp_line = line.lstrip().\
                            replace('\t', ' ').\
                            replace('\xe2\x80\x90', '').\
                            replace('\xe2\x80\x94', '-')
            ws_size = len(line) - len(tmp_line)
            ws = line[0:ws_size].replace('\t', '        ')
            line = "%s%s" % (ws, tmp_line)
            indent = len(line) - len(line.lstrip())
            # skip empty lines
            if len(line.strip()) == 0:
                continue
            # section names are uppercase
            if line.isupper():
                if line not in self.check_sections:
                    # if section not in check, reset variable
                    section_name = ''
                    continue
                else:
                    section_name = line
                    sections_dict[section_name] = list()
            else:
                if section_name == 'DESCRIPTION' or section_name == "OPTIONS":
                    # special parsing for DESCRIPTION and OPTIONS sections
                    # options usually start with -
                    if line.lstrip()[0] is '-' and line.lstrip()[1].isalpha():
                        option = line.lstrip()
                        indent = len(line) - len(line.lstrip())
                        position = len(sections_dict[section_name])
                        sections_dict[section_name].append(dict())
                        match = re.search(r' [A-Z]', option)

                        if match:
                            # there may be a description in-line
                            capital = match.group(0)
                            index = option.find(capital)
                            description = option[index+1:]
                            option = option[0:index].strip()
                            sections_dict[section_name][position][option] = list()
                            sections_dict[section_name][position][option].append(description)
                        else:
                            sections_dict[section_name][position][option] = list()
                        continue
                    elif position > -1 and (len(line) - len(line.lstrip())) >= indent:
                        sections_dict[section_name][position][option].append(line)
                        continue
                # end if DESCRIPTION or OPTIONS:
                if section_name != '':
                    sections_dict[section_name].append(line)
        return sections_dict

    def send_script_to_target(self):
        """ Sends getCommands script to selected server """
        scp_path = 'scp -P {0} {1}scripts/{2} {3}@{4}:~/{2} '.format(
            self.port,          # 0
            MEDIA_ROOT,         # 1
            self.script,        # 2
            self.user,          # 3
            self.host,          # 4
        )
        system = pexpect.spawn(scp_path)
        system.expect('password:')
        system.sendline(self.pssw)
        system.expect('100%', timeout=600)

    def get_script_results(self, path):
        """ Gets results file after commands test """
        # SSH Connection
        ssh = pxssh.pxssh(timeout=1000)
        ssh.login(self.host, self.user, self.pssw, port=self.port)
        ssh.prompt()
        # Make sure script can be executed
        ssh.sendline('chmod u+x ' + self.script)
        ssh.prompt()
        # Execute script
        ssh.sendline('./' + self.script + ' ' + path)
        ssh.prompt()
        filename = ssh.before.split('\r\n')[1]
        ssh.logout()
        print filename

        # Fetch results
        scp_path = 'scp -P {0} {1}@{2}:~/{3} {4}m-extract_results/{3}'.\
            format(
                self.port,
                self.user,
                self.host,
                filename,
                MEDIA_ROOT
            )
        system = pexpect.spawn(scp_path)
        system.expect('password:')
        system.sendline(self.pssw)
        system.expect('100%')

        # Read results
        read_command = "cat {0}m-extract_results/{1}".format(
            MEDIA_ROOT,
            filename
        )
        system = pexpect.spawn(read_command)
        system.expect('FILE_END')
        result = system.before
        all_commands = result.split("\r\n")
        return all_commands

    def run_parser(self, ok_commands=None):
        """ Loops ok_commands to get man docs for each one and adds to DB """
        # Failsafe check
        if ok_commands is None:
            return False
        manfile = self._create_manfile_on_target(ok_commands)
        man = self._get_manfile_from_target(manfile)
        all_man_files = man.split('END_MAN')
        for command in all_man_files:
            try:
                man_dict = self._man_as_dict(command.split('\n'))
                name = man_dict['NAME'][0].lstrip().split('-', 1)
                synopsis = man_dict['SYNOPSIS'][0].lstrip()
                options = man_dict['OPTIONS'] if 'OPTIONS' in man_dict \
                    else man_dict['DESCRIPTION']
                # prod = Product.objects.get(id=1)
                new_command, c_c = Command.objects.update_or_create(
                    name=name[0],
                    translate=name[0],
                    help_text=name[1],
                    manual_update=False
                )
                new_command.save()
                if options:
                    for opt in options:
                        if not isinstance(opt, dict):
                            continue
                        for key in opt:
                            comm_opt = key
                            h_t = ''.join(opt[key])
                            if comm_opt.find(' ') > -1:
                                c = comm_opt.split(' ', 1)
                                pref = c[0]
                                exp = c[1]
                                suff = True
                                if len(exp.split(' ')) > 2:
                                    suff = False
                                    exp = None
                                if exp and exp[0] is '-':
                                    suff = False
                                    exp = None
                            else:
                                pref = comm_opt
                                suff = False
                                exp = None
                            co, op_c = CommandOption.objects.update_or_create(
                                command=new_command,
                                prefix=pref,
                                suffix=suff,
                                expected=exp,
                                help_text=h_t)
                            co.save()
            # Just skip malformed data  
            except KeyError, e:
                pass
            except IndexError, e:
                pass
        # endfor
        return True
