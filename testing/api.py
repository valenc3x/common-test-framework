from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Min

from CTAFramework.models import *
from .json_to_robot import robot_generator
from .models import *

KEY = {
    # keyword
    'command': 'cm',
    'comment': 'cn',
    'for_in': 'fi',
    'for_range': 'fr',
    'keyword': 'kw',
    'tag': 'tg',
    'var_list': 'vl',
    'var_text': 'vt',
    # test_case
    'keyword_section': 'sk',
    'section': 'sc',
    'setting': 'st',
    # test suite
    'test_case': 'tc',
}


# API
@login_required
def get_user_products(request):
    all_products = Product.objects.all()
    json = dict()
    for product in all_products:
        json[product.id] = product.name
    return JsonResponse(json)


@login_required
def get_phases(request, p_id):
    all_phases = Phase.objects.filter(product__id=p_id)
    json = dict()
    for phase in all_phases:
        json[phase.id] = phase.name
    return JsonResponse(json)


@login_required
def get_repositories(request, p_id):
    all_repositories = TestCaseRepository.objects.filter(product__id=p_id)
    json = dict()
    for suite in all_repositories:
        json[suite.id] = suite.name
    return JsonResponse(json)


@login_required
def get_libraries(request, p_id):
    all_libs = CustomLibrary.objects.filter(product__id=p_id)
    json = dict()
    for lib in all_libs:
        json[lib.id] = lib.name
    return JsonResponse(json)


@login_required
def get_advanced_options(request):
    all_advanced = AdvancedOption.objects.all()
    json = list()
    for advanced in all_advanced:
        json.append({advanced.option: ''})
    return JsonResponse(json, safe=False)


# COMMANDS
def _get_command_items(queryset):
    json = list()
    for command in queryset:
        command_data = dict()
        command_data['type'] = 'item'
        command_data['info'] = {
            'key': KEY['command'],
            'name': command.name,
            'help_text': command.help_text,
        }
        all_options = CommandOption.objects.filter(command=command)
        command_data['values'] = list()
        for opt in all_options:
            command_data['values'].append({
                'id': opt.id,
                'command': opt.prefix or opt.option,
                'comment': opt.help_text or '',
                'required': opt.mandatory,
                'value': True if opt.mandatory else opt.expected or '',
                'input': 'checkbox' if not opt.suffix else 'text',
            })
        json.append(command_data)
    return json


@login_required
def get_product_commands(request, p_id):
    all_commands = Command.objects.filter(
        product__id=p_id,
        system_command=False
    )
    json = _get_command_items(all_commands)
    return JsonResponse(json, safe=False)


@login_required
def get_system_commands(request, p_id):
    all_commands = Command.objects.filter(
        product__id=p_id,
        system_command=True
    )
    json = _get_command_items(all_commands)
    return JsonResponse(json, safe=False)


# KEYWORDS
def _get_keyword_items(queryset):
    json = list()
    for keyword in queryset:
        keyword_data = dict()
        keyword_data['type'] = 'item'
        keyword_data['info'] = {
            'key': KEY['keyword'],
            'library_id': keyword.library.id,
            'name': str(keyword),
            'help_text': keyword.help_text,
        }

        all_options = KeywordOption.objects.filter(keyword=keyword)
        keyword_data['values'] = list()

        for opt in all_options:
            keyword_data['values'].append({
                'id': opt.id,
                'command': opt.name,
                'comment': '',
                'required': True if opt.default_value else False,
                'value': opt.default_value or '',
                'input': 'text',
            })

            if opt.name in ['name', '*keywords']:
                keyword_data['type'] = 'keyadd'
                keyword_data['innerdrop'] = [[]]
        json.append(keyword_data)
    return json


@login_required
def get_framework_keywords(request, p_id):
    rf = Product.objects.get(id=p_id).robot_version
    all_keywords = RobotKeyword.objects.filter(library__robot_version=rf)
    json = _get_keyword_items(all_keywords)
    return JsonResponse(json, safe=False)


@login_required
def get_custom_keywords(request, p_id):
    product_libraries = Product.objects.get(id=p_id).custom_libraries.all()
    all_keywords = CustomKeyword.objects.filter(library__in=product_libraries)
    json = _get_keyword_items(all_keywords)
    return JsonResponse(json, safe=False)


@login_required
def get_testcases(request, p_id):
    all_testcases = TestCase.objects.filter(repository__product_id=p_id)
    json = list()
    for tc in all_testcases:
        tc_data = dict()
        tc_data['type'] = 'suiteItem'
        tc_data['info'] = {
            'key': KEY['test_case'],
            'name': tc.name,
        }
        tc_data['values'] = {
            'command': tc.name,
            'comment': tc.help_text,
            'value': tc.robot_data,
            'required': True,
            'input': 'text'
        }
        json.append(tc_data)
    return JsonResponse(json, safe=False)


@login_required
def get_instance_data(request, model, obj_id):
    if model == 'CustomKeyword':
        obj_data = CustomKeyword.objects.filter(id=obj_id).annotate(
            product_id=Min('library__customlibrary__product')
        ).values()[0]
    elif model == 'TestCase':
        obj_data = TestCase.objects.filter(id=obj_id).annotate(
            product_id=Min('repository__product')
        ).values()[0]
    else:
        # Test Suite falls into default
        query = '%s.objects.filter(id=%s).values()[0]' % (model, obj_id)
        obj_data = eval(query)
    return JsonResponse(obj_data, safe=False)


@csrf_exempt
def get_preview(request, mode):
    if request.method == "POST":
        json = request.POST.get('json_data', False)
        if json:
            robot, args = robot_generator(json, mode)
            return JsonResponse({'text': robot, 'args': args}, safe=False)
    return JsonResponse({'error': 'Invalid format'}, safe=False)
