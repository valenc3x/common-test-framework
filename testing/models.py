import os

from django.db import models

from IBMauth.models import User


# COMMANDS
class Command(models.Model):
    """ General model for Commands objects.
    Is not actually used on any name ForeignKey, just as a generalization model
    """
    product = models.ManyToManyField(
        'CTAFramework.Product',
        limit_choices_to={'deprecated': False},
        help_text=("Choose all the products in which this command should run ",
                   "in a same way")
    )
    name = models.CharField(max_length=140)
    translate = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text="Type the traduction of the command (if apply)"
    )
    help_text = models.TextField(
        null=True,
        blank=True,
        help_text="Use this field to put any information about the command"
    )
    build_range = models.CharField(
        max_length=200,
        default="No restriction",
        null=True,
        blank=True,
        help_text=("Use this field to put a range of the builds in which ",
                   "this command works (e.g., 10-15)")
    )
    workaround = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text=("If build range exist, use this field for put the ",
                   "workaround version that should be loaded in case command ",
                   "doesn't exist into the build range")
    )
    manual_update = models.BooleanField(default=False)
    system_command = models.BooleanField(default=True)

    def __unicode__(self):
        allowed_versions_text = ""
        allowed_versions = self.product.filter(deprecated=False)
        if allowed_versions:
            allowed_versions = [p.name for p in allowed_versions]
            return "%s for %s" % (self.name, ", ".join(allowed_versions))


class CommandOption(models.Model):
    """Model for command options
    Reflects syntax for command usage, mostly identified using m-extract
    """
    command = models.ForeignKey(Command)
    option = models.CharField(
        max_length=200,
        help_text="You only should put the parameter name here."
    )
    help_text = models.TextField(
        null=True,
        blank=True,
        help_text="Use this field to put any information about this parameter."
    )
    prefix = models.CharField(
        null=True,
        blank=True,
        max_length=200,
        help_text=("Type the prefix of this command into the IBM product, ",
                   "e.g., -parameter or --paramX")
    )
    suffix = models.BooleanField(
        default=False,
        help_text="Check If option should receive a value for this parameter"
    )
    expected = models.CharField(
        null=True,
        blank=True,
        max_length=200,
        help_text="Use this to validate specific values for this parameter"
    )
    mandatory = models.BooleanField(
        default=False,
        help_text="Check this if you want this parameter appears as mandatory."
    )
    # TODO: Mostly unused fields
    dependent_of = models.TextField(
        null=True,
        blank=True,
        help_text=("Type the name of the parameters of which are dependent, ",
                   "e.g., parameter1, parameter2=True")
    )
    incompatibility = models.TextField(
        null=True,
        blank=True,
        help_text=("Type the name of the parameters which are not compatible ",
                   "with this one (in a comma separated way) ",
                   "e.g., parameter1, parameter2=True")
    )
    position = models.IntegerField(
        null=True,
        blank=True,
        help_text="Set to validate position of this parameter"
    )

    def __unicode__(self):
        return "%s [%s]" % (self.command, self.option or self.prefix or '')


class CommandRule(models.Model):
    """Model for command rule summary
    Reflects the general rules for using a command.
    """
    command = models.ForeignKey(Command)
    name = models.CharField(
        verbose_name="Rule name",
        max_length=200,
        help_text="This should be an identifier of the rule.")
    translate = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text=("Type the traduction of the command (if apply). ",
                   "If blank, translate of command will be taken.")
    )
    option = models.ForeignKey(
        CommandOption,
        help_text="This parameter will throw the rule."
    )
    help_text = models.TextField(
        null=True,
        blank=True,
        help_text=("Use this field to make difference with help texts. ",
                   "If blank, help text of command will be taken.")
    )
    mandatory_options = models.ManyToManyField(
        CommandOption,
        verbose_name="Mandatory options",
        null=True,
        blank=True,
        related_name="mandatory_options",
        help_text="Choose mandatory option for this rule."
    )
    optional = models.ManyToManyField(
        CommandOption,
        verbose_name="Optional",
        null=True,
        blank=True,
        related_name="optional",
        help_text="Choose optional parameters for this rule."
    )
    incompatibility = models.TextField(
        null=True,
        blank=True,
        help_text=("Syntax for indicate incompatibilities for this rule: ",
                   "param1:incom_param2|..|paramN:incom_paramI,incom_paramJ",
                   "If blank, it will take the default incompatibility rules")
    )
    dependencies = models.TextField(
        null=True,
        blank=True,
        help_text=("Syntax for indicate dependencies for this rule: ",
                   "param1:dependant_of_param2, dependant_of_param3|...|",
                   "paramN:dependant_of_paramI, dependant_of_paramJ', ",
                   "If blank, it will take the default incompatibility rules.")
    )

    def __unicode__(self):
        return self.name


class AdvancedOption(models.Model):
    """Model for Robot advanced options
    Robot Framework extended options on global scope
    """
    option = models.CharField(max_length=140)
    input_type = models.CharField(max_length=140, default="text")

    def __unicode__(self):
        return self.option


# ROBOT FRAMEWORK
class RobotLibrary(models.Model):
    """ Model to represent a Robot Library
    """
    name = models.CharField(max_length=140)
    description = models.TextField(blank=True, null=True)
    robot_version = models.ForeignKey(
        'CTAFramework.RobotFramework',
        related_name="libraries"
    )

    def _get_short_name(self):
        return self.name
    short_name = property(_get_short_name)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Robot Library"
        verbose_name_plural = "Robot Libraries"


class CustomLibrary(RobotLibrary):
    """ Child model for Custom libraries for Robot Framework
    """
    product = models.ForeignKey(
        'CTAFramework.Product',
        related_name="custom_libraries"
    )
    robot_data = models.TextField(editable=False)

    def _get_library_folder(self):
        return "%s/libraries/" % (
            self.product.name.lower().replace(' ', '_')
        )
    folder = property(_get_library_folder)

    def _get_library_path(self):
        return "%s/libraries/%s.robot" % (
            self.product.name.lower().replace(' ', '_'),
            self.name.replace(' ', '_')
        )
    path = property(_get_library_path)

    def __unicode__(self):
        return "%s's %s " % (self.product.name, self.name)

    class Meta:
        verbose_name = "Custom Library"
        verbose_name_plural = "Custom Libraries"


class RobotKeyword(models.Model):
    """ General model for Robot Keywords
    """
    name = models.CharField(max_length=140)
    help_text = models.CharField(max_length=140)
    docs = models.TextField(blank=True, null=True)
    library = models.ForeignKey(RobotLibrary, related_name='keywords')

    def __unicode__(self):
        return "%s.%s" % (self.library.short_name, self.name)


class CustomKeyword(RobotKeyword):
    """ Model for custom keywords
        Adds reference to its CustomLibrary and fields for json and robot text
    """
    json_data = models.TextField(editable=False)
    robot_data = models.TextField(editable=False)
    author = models.ForeignKey(User, related_name="keywords")

    def update_or_create_arguments(self, args_list):
        self.options.all().delete()
        for arg in args_list:
            def_value = ''
            if '=' in arg:
                def_value = arg.split('=')[1]
                arg = arg.split('=')[0]
            self.options.create(
                name=arg,
                default_value=def_value,
                keyword=self
            )

    def generate_robot_file(self):
        my_lib = CustomLibrary.objects.get(robotlibrary_ptr=self.library)
        prod = my_lib.product
        path = my_lib.path
        folder = prod.base_path + my_lib.folder
        # check or create target folder
        if not os.path.exists(folder):
            os.makedirs(folder)
        # build robot file
        file_content = '*** Keywords ***\n\n'
        all_keywords = CustomKeyword.objects.filter(library=my_lib)
        for keyword in all_keywords:
            file_content += keyword.robot_data

        with open(prod.base_path + path, 'w+') as f:
            f.writelines(file_content)
        my_lib.robot_data = file_content
        my_lib.save()


class KeywordOption(models.Model):
    """ Model to represent keyword arguments
    """
    name = models.CharField(max_length=140)
    keyword = models.ForeignKey(RobotKeyword, related_name="options")
    default_value = models.CharField(max_length=140, blank=True, null=True)

    def __unicode__(self):
        if self.default_value:
            return "%s %s='%s'" % (
                self.keyword,
                self.name,
                self.default_value
            )
        else:
            return "%s %s" % (self.keyword, self.name)


# TEST SCRIPTS
class TestCaseRepository(models.Model):
    """ Model to represent a collection of Test Cases
    """
    name = models.CharField(max_length=140)
    product = models.ForeignKey(
        'CTAFramework.Product',
        related_name="test_case_repositories"
    )
    description = models.TextField(blank=True, null=True)

    def _get_repository_folder(self):
        return "%s/test_cases/%s/" % (
            self.product.name.lower().replace(' ', '_'),
            self.name.replace(' ', '_')
        )
    folder = property(_get_repository_folder)

    def __unicode__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = "Test Case Repository"
        verbose_name_plural = "Test Case Repositories"


class TestCase(models.Model):
    """ Model to represent a test case
    Always belongs to a Test Suite Repository and integrates into Test Suites
    """
    name = models.CharField(max_length=140)
    help_text = models.CharField(max_length=140)
    repository = models.ForeignKey(
        TestCaseRepository,
        related_name="test_cases"
    )
    product_version = models.CharField(max_length=140)
    phase = models.ForeignKey('CTAFramework.Phase', related_name="test_cases")
    tags = models.CharField(max_length=140)
    author = models.ForeignKey(User, related_name="test_cases")
    json_data = models.TextField(editable=False)
    robot_data = models.TextField(editable=False)

    def _get_product_name(self):
        return self.repository.product.name
    product = property(_get_product_name)

    def _get_testcase_path(self):
        return "%s%s.robot" % (
            self.repository.folder,
            self.name.lower().replace(' ', '_')
        )
    path = property(_get_testcase_path)

    def __unicode__(self):
        return "%s.%s" % (self.repository, self.name)

    def save(self, *args, **kwargs):
        super(TestCase, self).save(*args, **kwargs)
        self._generate_robot_file()

    def _generate_robot_file(self):
        file_content = ''
        folder = self.repository.product.base_path + self.repository.folder
        path = self.repository.product.base_path + self.path

        # check or create target folder
        if not os.path.exists(folder):
            os.makedirs(folder)
        # build robot file
        file_content += self.robot_data
        file_content += '\n\n'

        with open(path, 'w+') as f:
            f.writelines(file_content)


class TestSuite(models.Model):
    """ Model to represent a test suite script
    Is generated using the Drag and Drop UI and creates a .robot file
    """
    name = models.CharField(max_length=140)
    product = models.ForeignKey(
        'CTAFramework.Product',
        related_name="test_suites"
    )
    help_text = models.CharField(max_length=140)
    product_version = models.CharField(max_length=140)
    json_data = models.TextField(editable=False)
    robot_data = models.TextField(editable=False)
    author = models.ForeignKey(User, related_name="test_suites")

    def __unicode__(self):
        return self.name
