from django import forms
from CTAFramework.models import Product


class MExtractForm(forms.Form):
    product = forms.ModelChoiceField(
        queryset=Product.objects.filter(deprecated=False),
        empty_label='Select a Product'
    )
    host = forms.CharField()
    port = forms.CharField(
        required=False,
        help_text='Default to 22'
    )
    usuario = forms.CharField(label='User')
    password = forms.CharField(widget=forms.PasswordInput)
    path = forms.CharField(
        required=False,
        help_text='Leave empty for system commands'
    )
