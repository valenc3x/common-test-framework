# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0006_auto_20161201_1425'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commandrule',
            options={},
        ),
        migrations.AlterField(
            model_name='commandrule',
            name='incompatibility',
            field=models.TextField(help_text=(b'Syntax for indicate incompatibilities for this rule: ', b'param1:incom_param2|..|paramN:incom_paramI,incom_paramJ', b'If blank, it will take the default incompatibility rules'), null=True, blank=True),
            preserve_default=True,
        ),
    ]
