# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('CTAFramework', '0003_auto_20161201_1425'),
        ('testing', '0007_auto_20161212_0131'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestCaseRepository',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('description', models.TextField(null=True, blank=True)),
                ('product', models.ForeignKey(related_name='test_case_repositories', to='CTAFramework.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='robottestsuite',
            name='author',
        ),
        migrations.RemoveField(
            model_name='robottestsuite',
            name='product',
        ),
        migrations.DeleteModel(
            name='RobotTestSuite',
        ),
        migrations.RemoveField(
            model_name='testcase',
            name='test_suite',
        ),
        migrations.RemoveField(
            model_name='testsuite',
            name='description',
        ),
        migrations.AddField(
            model_name='testcase',
            name='repository',
            field=models.ForeignKey(related_name='test_cases', default=1, to='testing.TestCaseRepository'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testsuite',
            name='author',
            field=models.ForeignKey(related_name='test_suites', default=2, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testsuite',
            name='json_data',
            field=models.TextField(default='', editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testsuite',
            name='robot_data',
            field=models.TextField(default='', editable=False),
            preserve_default=False,
        ),
    ]
