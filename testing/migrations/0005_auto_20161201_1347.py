# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0004_auto_20161201_1216'),
    ]

    operations = [
        migrations.RenameField(
            model_name='commandrule',
            old_name='parameter',
            new_name='option',
        ),
        migrations.RemoveField(
            model_name='commandrule',
            name='mandatory_parameters',
        ),
        migrations.RemoveField(
            model_name='commandrule',
            name='optional_parameters',
        ),
        migrations.AddField(
            model_name='command',
            name='system_command',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='mandatory_options',
            field=models.ManyToManyField(related_name='mandatory_options', to='testing.CommandOption', blank=True, help_text=b'Choose mandatory option for this rule.', null=True, verbose_name=b'Mandatory options'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='optional',
            field=models.ManyToManyField(related_name='optional', to='testing.CommandOption', blank=True, help_text=b'Choose optional parameters for this rule.', null=True, verbose_name=b'Optional'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='command',
            name='manual_update',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='commandoption',
            name='option',
            field=models.CharField(help_text=b'You only should put the parameter name here.', max_length=200),
            preserve_default=True,
        ),
    ]
