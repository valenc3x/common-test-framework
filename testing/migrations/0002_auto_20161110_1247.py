# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('testing', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RobotTestSuite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('json_data', models.TextField(editable=False)),
                ('robot_data', models.TextField(editable=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='testsuite',
            name='json_data',
        ),
        migrations.AddField(
            model_name='customkeyword',
            name='author',
            field=models.ForeignKey(related_name='keywords', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testcase',
            name='robot_data',
            field=models.TextField(default='', editable=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customkeyword',
            name='json_data',
            field=models.TextField(editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='testcase',
            name='json_data',
            field=models.TextField(editable=False),
            preserve_default=True,
        ),
    ]
