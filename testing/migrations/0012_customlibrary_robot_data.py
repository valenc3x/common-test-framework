# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0011_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='customlibrary',
            name='robot_data',
            field=models.TextField(default='', editable=False),
            preserve_default=False,
        ),
    ]
