# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('CTAFramework', '0002_auto_20161129_1205'),
        ('testing', '0003_auto_20161125_1254'),
    ]

    operations = [
        migrations.DeleteModel(
            name='FrameworkKeyword',
        ),
        migrations.DeleteModel(
            name='FrameworkLibrary',
        ),
        migrations.DeleteModel(
            name='ProductCommand',
        ),
        migrations.DeleteModel(
            name='SystemCommand',
        ),
        migrations.RemoveField(
            model_name='customkeyword',
            name='library',
        ),
        migrations.RemoveField(
            model_name='customlibrary',
            name='product',
        ),
        migrations.AddField(
            model_name='command',
            name='build_range',
            field=models.CharField(default=b'No restriction', max_length=200, null=True, help_text=(b'Use this field to put a range of the builds in which ', b'this command works (e.g., 10-15)'), blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='command',
            name='manual_update',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='command',
            name='product',
            field=models.ManyToManyField(help_text=(b'Choose all the products in which this command should run ', b'in a same way'), to='CTAFramework.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='command',
            name='workaround',
            field=models.CharField(help_text=(b'If build range exist, use this field for put the ', b'workaround version that should be loaded in case command ', b"doesn't exist into the build range"), max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='robotkeyword',
            name='library',
            field=models.ForeignKey(related_name='keywords', default=1, to='testing.RobotLibrary'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='robotlibrary',
            name='robot_version',
            field=models.ForeignKey(related_name='libraries', default=1, to='CTAFramework.RobotFramework'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='robottestsuite',
            name='author',
            field=models.ForeignKey(related_name='robot_test_suites', default=2, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='robottestsuite',
            name='product',
            field=models.ForeignKey(related_name='robot_test_suites', default=2, to='CTAFramework.Product'),
            preserve_default=False,
        ),
    ]
