# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0009_auto_20161220_1739'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='testcaserepository',
            options={'verbose_name': 'Test Case Repository', 'verbose_name_plural': 'Test Case Repositories'},
        ),
        migrations.AddField(
            model_name='testsuite',
            name='help_text',
            field=models.CharField(default='', max_length=140),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testsuite',
            name='product_version',
            field=models.CharField(default='', max_length=140),
            preserve_default=False,
        ),
    ]
