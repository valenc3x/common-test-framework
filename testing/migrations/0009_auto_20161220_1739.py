# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0003_auto_20161201_1425'),
        ('testing', '0008_auto_20161215_1607'),
    ]

    operations = [
        migrations.AddField(
            model_name='testcase',
            name='help_text',
            field=models.CharField(default='', max_length=140),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testcase',
            name='phase',
            field=models.ForeignKey(related_name='test_cases', default=1, to='CTAFramework.Phase'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testcase',
            name='product_version',
            field=models.CharField(default='', max_length=140),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='testcase',
            name='tags',
            field=models.CharField(default='', max_length=140),
            preserve_default=False,
        ),
    ]
