# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0009_auto_20161220_1739'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='testcaserepository',
            options={'verbose_name': 'Test Case Repository', 'verbose_name_plural': 'Test Case Repositories'},
        ),
    ]
