# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CTAFramework', '0003_auto_20161201_1425'),
        ('testing', '0005_auto_20161201_1347'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commandoption',
            options={},
        ),
        migrations.AlterModelOptions(
            name='customlibrary',
            options={'verbose_name': 'Custom Library', 'verbose_name_plural': 'Custom Libraries'},
        ),
        migrations.AlterModelOptions(
            name='robotlibrary',
            options={'verbose_name': 'Robot Library', 'verbose_name_plural': 'Robot Libraries'},
        ),
        migrations.AddField(
            model_name='customlibrary',
            name='product',
            field=models.ForeignKey(related_name='custom_libraries', default=2, to='CTAFramework.Product'),
            preserve_default=False,
        ),
    ]
