# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('CTAFramework', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvancedOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option', models.CharField(max_length=140)),
                ('input_type', models.CharField(default=b'text', max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Command',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('translate', models.CharField(help_text=b'Type the traduction of the command (if apply)', max_length=200, null=True, blank=True)),
                ('help_text', models.TextField(help_text=b'Use this field to put any information about the command', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CommandOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option', models.CharField(help_text=b'You only should put the parameter name here.', max_length=200, verbose_name=b'Parameter')),
                ('help_text', models.TextField(help_text=b'Use this field to put any information about this parameter.', null=True, blank=True)),
                ('prefix', models.CharField(help_text=(b'Type the prefix of this command into the IBM product, ', b'e.g., -parameter or --paramX'), max_length=200, null=True, blank=True)),
                ('suffix', models.BooleanField(default=False, help_text=b'Check If option should receive a value for this parameter')),
                ('expected', models.CharField(help_text=b'Use this to validate specific values for this parameter', max_length=200, null=True, blank=True)),
                ('mandatory', models.BooleanField(default=False, help_text=b'Check this if you want this parameter appears as mandatory.')),
                ('dependent_of', models.TextField(help_text=(b'Type the name of the parameters of which are dependent, ', b'e.g., parameter1, parameter2=True'), null=True, blank=True)),
                ('incompatibility', models.TextField(help_text=(b'Type the name of the parameters which are not compatible ', b'with this one (in a comma separated way) ', b'e.g., parameter1, parameter2=True'), null=True, blank=True)),
                ('position', models.IntegerField(help_text=b'Set to validate position of this parameter', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Command parameter',
                'verbose_name_plural': 'Command parameters',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CommandRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'This should be an identifier of the rule.', max_length=200, verbose_name=b'Rule name')),
                ('translate', models.CharField(help_text=(b'Type the traduction of the command (if apply). ', b'If blank, translate of command will be taken.'), max_length=200, null=True, blank=True)),
                ('help_text', models.TextField(help_text=(b'Use this field to make difference with help texts. ', b'If blank, help text of command will be taken.'), null=True, blank=True)),
                ('incompatibility', models.TextField(help_text=(b'Syntax for indicate incompatibilities for this rule: ', b'param1:incomp_param2|..|paramN:incomp_paramI,incomp_paramJ', b'If blank, it will take the default incompatibility rules'), null=True, blank=True)),
                ('dependencies', models.TextField(help_text=(b'Syntax for indicate dependencies for this rule: ', b'param1:dependant_of_param2, dependant_of_param3|...|', b"paramN:dependant_of_paramI, dependant_of_paramJ', ", b'If blank, it will take the default incompatibility rules.'), null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Rule',
                'verbose_name_plural': 'Command rules',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KeywordOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('default_value', models.CharField(max_length=140, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductCommand',
            fields=[
                ('command_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.Command')),
                ('build_range', models.CharField(default=b'No restriction', max_length=200, null=True, help_text=(b'Use this field to put a range of the builds in which ', b'this command works (e.g., 10-15)'), blank=True)),
                ('workaround', models.CharField(help_text=(b'If build range exist, use this field for put the ', b'workaround version that should be loaded in case command ', b"doesn't exist into the build range"), max_length=200, null=True, blank=True)),
                ('product', models.ManyToManyField(help_text=(b'Choose all the products in which this command should run ', b'in a same way'), to='CTAFramework.Product')),
            ],
            options={
            },
            bases=('testing.command',),
        ),
        migrations.CreateModel(
            name='RobotKeyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('help_text', models.CharField(max_length=140)),
                ('docs', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FrameworkKeyword',
            fields=[
                ('robotkeyword_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.RobotKeyword')),
            ],
            options={
            },
            bases=('testing.robotkeyword',),
        ),
        migrations.CreateModel(
            name='CustomKeyword',
            fields=[
                ('robotkeyword_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.RobotKeyword')),
                ('json_data', models.TextField()),
                ('robot_data', models.TextField(editable=False)),
            ],
            options={
            },
            bases=('testing.robotkeyword',),
        ),
        migrations.CreateModel(
            name='RobotLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FrameworkLibrary',
            fields=[
                ('robotlibrary_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.RobotLibrary')),
                ('robot_version', models.ForeignKey(related_name='libraries', to='CTAFramework.RobotFramework')),
            ],
            options={
            },
            bases=('testing.robotlibrary',),
        ),
        migrations.CreateModel(
            name='CustomLibrary',
            fields=[
                ('robotlibrary_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.RobotLibrary')),
                ('product', models.ForeignKey(related_name='robot_libraries', to='CTAFramework.Product')),
            ],
            options={
            },
            bases=('testing.robotlibrary',),
        ),
        migrations.CreateModel(
            name='SystemCommand',
            fields=[
                ('command_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='testing.Command')),
                ('manual_update', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=('testing.command',),
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('json_data', models.TextField()),
                ('author', models.ForeignKey(related_name='test_cases', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TestSuite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('json_data', models.TextField()),
                ('product', models.ForeignKey(related_name='test_suites', to='CTAFramework.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='testcase',
            name='test_suite',
            field=models.ForeignKey(related_name='test_cases', to='testing.TestSuite'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='keywordoption',
            name='keyword',
            field=models.ForeignKey(related_name='options', to='testing.RobotKeyword'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='frameworkkeyword',
            name='library',
            field=models.ForeignKey(related_name='keywords', to='testing.FrameworkLibrary'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customkeyword',
            name='library',
            field=models.ForeignKey(related_name='keywords', to='testing.CustomLibrary'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='command',
            field=models.ForeignKey(to='testing.Command'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='mandatory_parameters',
            field=models.ManyToManyField(related_name='mandatory_parameters', to='testing.CommandOption', blank=True, help_text=b'Choose mandatory parameters for this rule.', null=True, verbose_name=b'Mandatory parameters'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='optional_parameters',
            field=models.ManyToManyField(related_name='optional_optional', to='testing.CommandOption', blank=True, help_text=b'Choose optional parameters for this rule.', null=True, verbose_name=b'Optional parameters'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandrule',
            name='parameter',
            field=models.ForeignKey(help_text=b'This parameter will throw the rule.', to='testing.CommandOption'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commandoption',
            name='command',
            field=models.ForeignKey(to='testing.Command'),
            preserve_default=True,
        ),
    ]
