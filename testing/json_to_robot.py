import json

BREAK = '\n'
BLANK = ' '
EMPTY = ''
EQUAL = '='
COMMA = ','
DOT = '.'
TAB = BLANK*4
PAD = TAB + '#'
BACKSLASH = TAB + '\\'
OPEN_VARIABLE_TEXT = '${'
OPEN_VARIABLE_LIST = '@{'
CLOSE_VARIABLE = '}'
FOR_OPEN = TAB + ':FOR' + TAB
FOR_IN = TAB + 'IN' + TAB
FOR_RANGE = TAB + 'IN RANGE' + TAB
COMMAND_CUSTOM = '$python'
VALUE_EMPTY = '${EMPTY}'

SETTINGS = '*** Settings ***' + BREAK*2
VARIABLES = BREAK + '*** Variables ***' + BREAK*2
TEST_CASES = BREAK + '*** Test Cases ***' + BREAK*2
KEYWORDS = BREAK + '*** Keywords ***' + BREAK*2

DOCUMENTATION = 'documentation'
ARGUMENTS = 'arguments'
TAG = 'tag'
RETURN = 'return'

TAG_DOCUMENTATION = TAB + '[Documentation]' + TAB
TAG_ARGUMENTS = TAB + '[Arguments]' + BLANK*8
TAG_TAG = TAB + '[Tag]' + BLANK*14
TAG_RETURN = TAB + '[Return]' + BLANK*11
TAG_BLANK = TAB + DOT*3 + BLANK*16

KEY_TAG = 'tg'
KEY_KEYWORD = 'kw'
KEY_COMMAND = 'cm'
KEY_VARIABLE_TEXT = 'vt'
KEY_VARIABLE_LIST = 'vl'
KEY_COMMENT = 'cn'
KEY_FOR_IN = 'fi'
KEY_FOR_RANGE = 'fr'

KEYWORD_INFO = 'info'
KEYWORD_KEY = 'key'
KEYWORD_NAME = 'name'
KEYWORD_HEADER = 'namekeyword'
KEYWORD_DESCRIPTION = 'descriptionkeyword'
KEYWORD_VALUES = 'values'
KEYWORD_COMMAND = 'command'
KEYWORD_TAGS = 'tags'
KEYWORD_VALUE = 'value'
KEYWORD_TEXT = 'text'
KEYWORD_REQUIRED = 'required'
KEYWORD_INDEX = 'condition1'
KEYWORD_ITERATOR = 'condition3'
KEYWORD_INNER_DROP = 'innerdrop'
KEYWORD_BLANK = TAB + DOT*3 + TAB

RUN_KEYWORD = 'Run Keyword'

ARGUMENT_LIST = list()


def appendValue(value):
    return EMPTY if value == EMPTY else TAB + value


def equalValue(value):
    return EQUAL + VALUE_EMPTY if value == EMPTY else EQUAL + value


def appendTag(tag):
    if tag == DOCUMENTATION:
        return TAG_DOCUMENTATION
    elif tag == ARGUMENTS:
        return TAG_ARGUMENTS
    elif tag == TAG:
        return TAG_TAG
    else:
        return TAG_RETURN


def tagFormat(keywordName, value, lastValue):
    script = value[KEYWORD_TEXT]
    if keywordName == ARGUMENTS or keywordName == RETURN:
        script += EMPTY if value == lastValue else TAB
    else:
        script += EMPTY if value == lastValue else BREAK + TAG_BLANK
    return script


def extractTag(keyword):
    global ARGUMENT_LIST
    script = EMPTY
    try:
        if keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_TAG:
            script += appendTag(keyword[KEYWORD_VALUES][0][KEYWORD_COMMAND].lower())
            for value in keyword[KEYWORD_VALUES][0][KEYWORD_TAGS]:
                if keyword[KEYWORD_VALUES][0][KEYWORD_COMMAND].lower() == ARGUMENTS:
                    ARGUMENT_LIST.append(value[KEYWORD_TEXT])
                script += tagFormat(
                    keyword[KEYWORD_VALUES][0][KEYWORD_COMMAND].lower(),
                    value,
                    keyword[KEYWORD_VALUES][0][KEYWORD_TAGS][len(keyword[KEYWORD_VALUES][0][KEYWORD_TAGS]) - 1]
                )
            script += BREAK
            return script
    except Exception as exception:
        script += PAD + 'Error to extract TAG, JSON script have inconsistent data' + BREAK
    return script


def keywordFormat(keyword, value):
    if value[KEYWORD_REQUIRED]:
        return appendValue(value[KEYWORD_COMMAND]) + equalValue(value[KEYWORD_VALUE])
    elif RUN_KEYWORD in keyword[KEYWORD_INFO][KEYWORD_NAME] and value[KEYWORD_VALUE] == EMPTY:
        return EMPTY
    else:
        return TAB + VALUE_EMPTY if value[KEYWORD_VALUE] == EMPTY else appendValue(value[KEYWORD_VALUE])


def commandFormat(keyword, value):
    if value[KEYWORD_VALUE] == EMPTY:
        return appendValue(value[KEYWORD_COMMAND]) if keyword[KEYWORD_INFO][KEYWORD_NAME] == COMMAND_CUSTOM else appendValue(value[KEYWORD_VALUE])
    else:
        return appendValue(value[KEYWORD_COMMAND]) + appendValue(value[KEYWORD_VALUE])


def keywordOrCommandFormat(keyword, value):
    if keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_KEYWORD:
        return keywordFormat(keyword, value)
    else:
        return appendValue(value[KEYWORD_COMMAND]) if value[KEYWORD_VALUE] else commandFormat(keyword, value)


def extractKeywordOrCommand(keyword, prefix):
    script = EMPTY
    try:
        if (keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_KEYWORD or
                keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_COMMAND):
            script += EMPTY if keyword[KEYWORD_INFO][KEYWORD_NAME] == COMMAND_CUSTOM else TAB + keyword[KEYWORD_INFO][KEYWORD_NAME]
            for value in keyword[KEYWORD_VALUES]:
                    script += keywordOrCommandFormat(keyword, value)
            script += BREAK
            if KEYWORD_INNER_DROP in keyword and len(keyword[KEYWORD_INNER_DROP][0]) > 0 :
                for innerKeyword in keyword[KEYWORD_INNER_DROP][0]:
                    script += prefix + extractInnerKeywords(innerKeyword, False)
            return script
    except Exception as exception:
        script += PAD + 'Error to extract KEYWORD OR COMMAND, JSON script have inconsistent data' + BREAK
    return script


def variableFormat(keywordKey, value, lastValue):
    script = value[KEYWORD_VALUE]
    if keywordKey == KEY_VARIABLE_TEXT:
        return EMPTY if value == lastValue else TAB
    else:
        return EMPTY if value == lastValue else COMMA


def extractVariables(keyword):
    script = EMPTY
    try:
        if (keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_VARIABLE_TEXT or
                keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_VARIABLE_LIST):
            script += TAB
            script += OPEN_VARIABLE_TEXT if keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_VARIABLE_TEXT else OPEN_VARIABLE_LIST
            script += keyword[KEYWORD_VALUES][0][KEYWORD_COMMAND] + CLOSE_VARIABLE + EQUAL + TAB
            if KEYWORD_INNER_DROP in keyword and len(keyword[KEYWORD_INNER_DROP][0]) > 0:
                script += BREAK
                for innerKeyword in keyword[KEYWORD_INNER_DROP][0]:
                    script += extractInnerKeywords(innerKeyword, False)
            else:
                for value in keyword[KEYWORD_VALUES]:
                    script += variableFormat(
                        keyword[KEYWORD_INFO][KEYWORD_KEY],
                        value,
                        keyword[KEYWORD_VALUES][len(keyword[KEYWORD_VALUES]) - 1]
                    )
            script += BREAK
            return script
    except Exception as exception:
        script += PAD + 'Error to extract VARIABLES, JSON script have inconsistent data' + BREAK
    return script


def extractComment(keyword):
    script = EMPTY
    try:
        if keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_COMMENT:
            script += BREAK
            for value in keyword[KEYWORD_VALUES]:
                script += PAD + value[KEYWORD_COMMAND] + BREAK
            return script
    except Exception as exception:
        script += PAD + 'Error to extract COMMENT, JSON script have inconsistent data' + BREAK
    return script


def extractInnerKeywords(innerKeyword, isFor):
    script = EMPTY
    if isFor:
        script += EMPTY if extractKeywordOrCommand(innerKeyword, BACKSLASH) == EMPTY else BACKSLASH + extractKeywordOrCommand(innerKeyword, BACKSLASH)
        script += EMPTY if extractVariables(innerKeyword) == EMPTY else BACKSLASH + extractVariables(innerKeyword)
    else:
        script += EMPTY if extractKeywordOrCommand(innerKeyword, EMPTY) == EMPTY else KEYWORD_BLANK + extractKeywordOrCommand(innerKeyword, EMPTY)
        script += EMPTY if extractVariables(innerKeyword) == EMPTY else KEYWORD_BLANK + extractVariables(innerKeyword)
    return script


def extractFor(keyword):
    script = EMPTY
    try:
        if (keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_FOR_IN or
                keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_FOR_RANGE):
            script += FOR_OPEN + keyword[KEYWORD_VALUES][0][KEYWORD_INDEX]
            script += FOR_IN if keyword[KEYWORD_INFO][KEYWORD_KEY] == KEY_FOR_IN else FOR_RANGE
            script += keyword[KEYWORD_VALUES][0][KEYWORD_ITERATOR] + BREAK
            for innerKeyword in keyword[KEYWORD_INNER_DROP][0]:
                script += extractInnerKeywords(innerKeyword, True)
            return script
    except Exception as exception:
        script += PAD + 'Error to extract FOR, JSON script have inconsistent data' + BREAK
    return script


def extractKeywordHeaders(keyword):
    try:
        return keyword[KEYWORD_HEADER] + BREAK + TAG_DOCUMENTATION + keyword[KEYWORD_DESCRIPTION] + BREAK
    except Exception as exception:
        return PAD + 'Error to extract KEYWORD HEADERS, JSON script have inconsistent data' + BREAK


def extractTestCaseHeaders(keyword):
    try:
        return TEST_CASES + keyword[KEYWORD_HEADER] + BREAK + TAG_DOCUMENTATION + keyword[KEYWORD_DESCRIPTION] + BREAK
    except Exception as exception:
        return PAD + 'Error to extract TEST CASE HEADERS, JSON script have inconsistent data' + BREAK


def extractElements(keyword):
    keywordScript = EMPTY
    keywordScript += extractTag(keyword)
    keywordScript += extractKeywordOrCommand(keyword, EMPTY)
    keywordScript += extractVariables(keyword)
    keywordScript += extractComment(keyword)
    keywordScript += extractFor(keyword)
    return keywordScript


def keywordDefinition(jsonString, mode):
    global ARGUMENT_LIST
    keywordScript = EMPTY
    json_data = json.loads(jsonString)
    for keyword in json_data:
        if keyword == json_data[0]:
            if mode == '1':
                keywordScript += extractKeywordHeaders(keyword)
            else:
                keywordScript += extractTestCaseHeaders(keyword)
        else:
            keywordScript += extractElements(keyword)
    keywordScript += BREAK*2
    return keywordScript, ARGUMENT_LIST


def robot_generator(json_text, mode):
    global ARGUMENT_LIST
    ARGUMENT_LIST = list()
    return keywordDefinition(json_text, mode)
