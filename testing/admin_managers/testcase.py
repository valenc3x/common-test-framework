from django.conf import settings
from django.shortcuts import redirect, render

import pexpect
from CTAFramework.models import Phase
from IBMadmin import IBMAdmin
from testing.models import CustomLibrary, TestCase, TestCaseRepository
from types import MethodType

try:
    from pexpect import pxssh
except Exception, e:
    def spawn(self):
        """"Not supported function error"""
        print 'Not supported'
    pexpect.spawn = MethodType(spawn, pexpect, pexpect)


class TestCaseAdmin(IBMAdmin):
    list_display = ['__unicode__', 'product', 'product_version', 'help_text']

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == "POST":
            # TODO: do form validation
            tc_repository = TestCaseRepository.objects.get(
                id=request.POST['repository']
            )
            tc_phase = Phase.objects.get(id=request.POST['phase'])
            tc = TestCase(
                name=request.POST['name'],
                help_text=request.POST['help_text'],
                repository=tc_repository,
                product_version=request.POST['product_version'].strip(),
                phase=tc_phase,
                tags=request.POST['tags'].strip(),
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            tc.save()
            if request.POST.get('run', None):
                run = self.run_on_target(
                    tc,
                    request.POST.get('host', None),
                    request.POST.get('user', None),
                    request.POST.get('passw', None),
                    request.POST.get('used_libraries', None)
                )

            return redirect('/admin/testing/testcase/')
        # else:
        return render(request, 'testing/drag_drop.html', {
            'title': 'New Test Case',
        })

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if request.method == "POST":
            # TODO: do form validation
            tc_repository = TestCaseRepository.objects.get(
                id=request.POST['repository']
            )
            tc_phase = Phase.objects.get(id=request.POST['phase'])
            tc = TestCase(
                id=request.POST.get('id', None),
                name=request.POST['name'],
                help_text=request.POST['help_text'],
                repository=tc_repository,
                product_version=request.POST['product_version'],
                phase=tc_phase,
                tags=request.POST['tags'],
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            tc.save()
            if request.POST.get('run', None):
                run = self.run_on_target(
                    tc,
                    request.POST.get('host', None),
                    request.POST.get('user', None),
                    request.POST.get('passw', None),
                    request.POST.get('used_libraries', None)
                )

            return redirect('/admin/testing/testcase/')

        return render(request, 'testing/drag_drop.html', {
            'title': 'Edit Test Case',
            'edit_mode': True,
            'model': 'TestCase',
            'object_id': object_id
        })

    def run_on_target(self, testcase, host, user, passw, libs):
        """ Copies robot data on target server and runs robot process
        """
        if None in (host, user, passw):
            return False

        # inner Helper function to send files to target server
        def send_file_to_server(filename, host, user, passw):
            scp_path = 'scp -o StrictHostKeyChecking=no {0} {1}@{2}:~/ '.format(
                filename,
                user,
                host,
            )
            system = pexpect.spawn(scp_path)
            system.expect('password:')
            system.sendline(passw)
            system.expect('100%', timeout=600)

        # Send testcase to server
        send_file_to_server(
            testcase.repository.product.base_path + testcase.path,
            host,
            user,
            passw
        )
        # Get robot filename to run pybot command later
        robot_file = testcase.path.split('/')[-1]
        print libs
        print "was libs"
        print robot_file
        # get each keywork library used in testcase and send them to server
        for lib_id in libs:
            print lib_id
            lib_id = lib_id if lib_id.strip().isdigit() else 0
            print lib_id
            library = CustomLibrary.objects.filter(id=lib_id).first()
            if library:
                send_file_to_server(
                    library.product.base_path + library.path,
                    host,
                    user,
                    passw
                )
        print "before pybot"
        # Conect via ssh and run pybot command
        ssh = pxssh.pxssh(timeout=1000)
        ssh.login(host, user, passw)
        ssh.prompt()
        # Make sure script can be executed
        run_keyword = 'pybot -P ./ %s' % robot_file
        print run_keyword
        ssh.sendline(run_keyword)
        ssh.prompt()
        ssh.logout()
