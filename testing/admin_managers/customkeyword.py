from django.shortcuts import redirect, render

from IBMadmin import IBMAdmin
from testing.models import CustomKeyword, CustomLibrary, KeywordOption


class CustomKeywordAdmin(IBMAdmin):
    list_display = ['__unicode__', 'help_text']

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == "POST":
            # TODO: do form validation
            ck = CustomKeyword(
                name=request.POST['name'],
                help_text=request.POST['help_text'],
                library=CustomLibrary.objects.get(id=request.POST['library']),
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            ck.save()
            ck.update_or_create_arguments(
                request.POST['customarguments'].split(',')
            )
            ck.generate_robot_file()
            return redirect('/admin/testing/customkeyword/')
        # else:
        return render(request, 'testing/drag_drop.html', {
            'title': 'New Keyword',
        })

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if request.method == "POST":
            # TODO: do form validation
            ck = CustomKeyword(
                id=request.POST.get('id', None),
                name=request.POST['name'],
                help_text=request.POST['help_text'],
                library=CustomLibrary.objects.get(id=request.POST['library']),
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            ck.save()
            ck.update_or_create_arguments(
                request.POST['customarguments'].split(',')
            )
            ck.generate_robot_file()
            return redirect('/admin/testing/customkeyword/')

        return render(request, 'testing/drag_drop.html', {
            'title': 'Edit Keyword',
            'edit_mode': True,
            'model': 'CustomKeyword',
            'object_id': object_id
        })
