from django.shortcuts import redirect, render

from django.shortcuts import render
from CTAFramework.models import Product
from IBMadmin import IBMAdmin
from testing.models import TestSuite


class TestSuiteAdmin(IBMAdmin):
    list_display = ['name', 'product', 'product_version']

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == "POST":
            tc = TestSuite(
                name=request.POST['name'],
                product=Product.objects.get(id=request.POST['product']),
                help_text=request.POST['help_text'],
                product_version=request.POST['product_version'],
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            tc.save()
            return redirect('/admin/testing/testsuite')
        return render(request, 'testing/drag_drop.html', {
            'title': 'New Test Suite',
        })

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if request.method == "POST":
            tc = TestSuite(
                id=request.POST.get('id', None),
                name=request.POST['name'],
                product=Product.objects.get(id=request.POST['product']),
                help_text=request.POST['help_text'],
                product_version=request.POST['product_version'],
                json_data=request.POST['json_data'],
                robot_data=request.POST['robot_data'],
                author=request.user
            )
            tc.save()
            return redirect('/admin/testing/testsuite')

        return render(request, 'testing/drag_drop.html', {
            'title': 'Edit Test Suite',
            'edit_mode': True,
            'model': 'TestSuite',
            'object_id': object_id
        })
