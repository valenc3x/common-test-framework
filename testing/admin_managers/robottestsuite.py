from django.shortcuts import redirect, render

from IBMadmin import IBMAdmin


class RobotTestSuiteAdmin(IBMAdmin):
    def add_view(self, request, form_url='', extra_context=None):
        return render(request, 'testing/drag_drop.html', {
            'title': 'New Robot Test Suite',
        })

    def change_view(self, request, object_id, form_url='', extra_context=None):
        return render(request, 'testing/drag_drop.html', {
            'title': 'Edit Robot Test Suite',
            'edit_mode': True,
            'model': 'RobotTestSuite',
            'object_id': object_id
        })

