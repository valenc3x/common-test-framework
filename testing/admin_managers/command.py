from django.conf.urls import url
from django.contrib.admin import StackedInline
from django.db import models
from django.forms import Textarea
from django.shortcuts import render

import pexpect
from CTAFramework.utils import run_on_thread
from IBMadmin import IBMAdmin
from testing.forms import MExtractForm
from testing.mextract import MExtract
from testing.models import CommandOption, CommandRule
from types import MethodType

try:
    from pexpect import pxssh
except Exception, e:
    def spawn(self):
        """" Not supported function error for Windows """
    pexpect.spawn = MethodType(spawn, pexpect, pexpect)


LOGIN_ERROR = -1
SERVER_ERROR = -2


class CommandOptionInline(StackedInline):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }
    model = CommandOption
    extra = 1


class CommandRuleInline(StackedInline):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }

    filter_horizontal = ['mandatory_parameters', 'optional_parameters']
    model = CommandRule
    extra = 1

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        cmd_id = request.path.split("/")[-2]
        if db_field.name == "parameter":
            kwargs["queryset"] = CommandOption.objects.filter(command=cmd_id)
        return super(CommandRuleInline, self).\
            formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        cmd_id = request.path.split("/")[-2]
        if db_field.name == "mandatory_parameters":
            kwargs["queryset"] = CommandOption.objects.filter(command=cmd_id)
        elif db_field.name == "optional_parameters":
            kwargs["queryset"] = CommandOption.objects.filter(command=cmd_id)
        return super(CommandRuleInline, self).\
            formfield_for_manytomany(db_field, request, **kwargs)


class CommandAdmin(IBMAdmin):
    list_display = ['name', '__unicode__', 'system_command']
    list_filter = ['product']
    filter_horizontal = ('product',)
    inlines = [CommandOptionInline, ]

    def get_urls(self):
        urls = super(CommandAdmin, self).get_urls()
        new_urls = [
            url(r'^mextract/$', self.import_mextract, name="m_extract"),
        ]
        return new_urls + urls

    def change_view(self, request, object_id, form_url='', extra_context=None):
        inlines = self.inlines
        if CommandRuleInline not in inlines:
            self.inlines = [CommandRuleInline] + inlines
        return super(CommandAdmin, self).\
            changeform_view(request, object_id, form_url, extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        self.inlines = [CommandOptionInline]
        return super(CommandAdmin, self).\
            add_view(request, form_url, extra_context)

    def import_mextract(self, request):
        """ Manages import M Extract view
        """
        result = dict()
        result['status'] = False
        if request.method == 'POST':
            form = MExtractForm(request.POST)
            if form.is_valid():
                cleaned = form.cleaned_data

                # cleaned up ssh login data
                ssh_data = {
                    'user': cleaned['usuario'],
                    'host': cleaned['host'],
                    'pass': cleaned['password'],
                    'port': cleaned['port'] or '22',
                    'path': cleaned['path'] or ''
                }
                # save ssh connection data to session
                request.session['ssh_data'] = ssh_data

                man_parser = MExtract()
                man_parser.set_ssh_info(
                    ssh_data['user'],
                    ssh_data['host'],
                    ssh_data['pass'],
                    ssh_data['port']
                )

                commands = self.get_command_list(man_parser, ssh_data['path'])
                if isinstance(commands, int):
                    if commands == LOGIN_ERROR:
                        form.add_error('password', 'SSH Login failed')
                    elif commands == SERVER_ERROR:
                        form.add_error('host', 'Server Error')
                else:
                    result['status'] = True
                    result['list'] = commands
                    ok_count = 0
                    fail_count = 0
                    warning_count = 0
                    command_count = len(commands)
                    all_ok = list()
                    for line in commands:
                        if 'Ok' in line:
                            ok_count += 1
                            all_ok.append(line[1])
                        if 'Fail' in line:
                            fail_count += 1
                        if 'warning' in line:
                            warning_count += 1
                    result['summary'] = {
                        'ok_count': ok_count,
                        'fail_count': fail_count,
                        'warning_count': warning_count,
                        'command_count': command_count
                    }
                    # Actually run m-extract with ok commands
                    self.run_mextract(man_parser, all_ok)
        else:
            form = MExtractForm()

        template_vars = {
            "title": "Import from M-Extract",
            "form": form,
            "result": result,
        }
        return render(
            request,
            'admin/testing/command/import_mextract.html',
            template_vars
        )

    def get_command_list(self, man_parser, path):
        """ Sends script to target, runs it and fetches results
        """
        try:
            # Copy script to remote server
            man_parser.send_script_to_target()
            all_commands = man_parser.get_script_results(path)
            # get lines except last
            return [r.split() for r in all_commands[0:-1:]]
        except pxssh.ExceptionPxssh as error:
            print "pxssh failed on login.", error
            return LOGIN_ERROR
        # except Exception as error:
        #     print 'ERROR', error
        #     return SERVER_ERROR

    # @run_on_thread
    # FIX: Running this on a thread seems to make it fail/not run.
    def run_mextract(self, man_parser, ok_commands):
        """ runs actual M Extract function
        """
        # TODO: show process is in background
        done = man_parser.run_parser(ok_commands)

        if done:
            # TODO: somehow notify process finished
            pass
