from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns(
    '',
    url(r'^CommandOptions/$', CommandOptionList.as_view(), name='command-option-list'),
    url(r'^CommandRules$', CommandRuleList.as_view()),
    url(r'^TestCases$', TestCaseList.as_view(), name='test-case-list'),
    url(r'^TestSuites$', TestSuiteList.as_view(), name='test-suite-list'),
    url(r'^CommandOptionsCreate$', CommandOptionCreate.as_view()),
    url(r'^CommandOptionsUpdate/(?P<pk>\d+)/$', CommandOptionUpdate.as_view()),
    # url(r'^CommandOptionsDelete/(?P<pk>\d+)/$', CommandOptionDelete.as_view()),
    # url(r'^CommandOptions/(?P<pk>\d+)/$', CommandOptionDetailView.as_view(), name='command-option-detail'),
    # url(r'^CommandRules/(?P<pk>\d+)/$', CommandRuleDetailView.as_view(), name='command-rules-detail'),

    # DRAG and DROP
    # url(r'^admin/testing/keyword$', new_keyword, name='new_keyword'),
    # url(r'^admin/testing/testcase$', new_testcase, name='new_testcase'),
)


