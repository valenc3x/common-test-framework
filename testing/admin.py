import IBMadmin as admin

from .models import *
from .admin_managers.command import CommandAdmin
from .admin_managers.customkeyword import CustomKeywordAdmin
from .admin_managers.testcase import TestCaseAdmin
from .admin_managers.testsuite import TestSuiteAdmin

# Commands
admin.site.register(Command, CommandAdmin)
admin.site.register(AdvancedOption)

# Stores
admin.site.register(CustomLibrary)
admin.site.register(TestCaseRepository)

# Drag and Drop
admin.site.register(CustomKeyword, CustomKeywordAdmin)
admin.site.register(TestCase, TestCaseAdmin)
admin.site.register(TestSuite, TestSuiteAdmin)
