#!/bin/bash

# Simple script para obtencion de comandos con standard de documentacion
# Creado por: Arturo J Lopez Fausto
# Version: 1.5

function probarComando(){
    value=1
    # check if command has manpage
    man_page=$(man $1 2>>/dev/null | col -b )
    n="ok"
    s="ok"
    d="ok"
    sa="ok"
    if [[ "$man_page" == "" ]]; then
        echo "ERROR:   No MANPAGE     for $1" >> $2
        n="fail"
        s="fail"
        d="fail"
        sa="fail"
        value=0
    else
        # Check for manpage sections and write to errors
        comando=$(echo "$man_page" | grep -e NOMBRE -e NAME)
        if [ "$comando" == "" ]; then
            echo "ERROR:   No NAME        for $1" >> $2
            n="fail"
            value=0
        fi
        comando=$(echo "$man_page" | grep -e SINOPSIS -e SYNOPSIS)
        if [ "$comando" == "" ]; then
            echo "WARNING: No SYNOPSIS    for $1" >> $2
            s="fail"
            value=0
        fi
        comando=$(echo "$man_page" | grep -e DESCRIPCIÓN -e DESCRIPTION)
        if [ "$comando" == "" ]; then
            echo "WARNING: No DESCRIPTION for $1" >> $2
            d="warning"
        fi
        comando=$(echo "$man_page" | grep -e "VÉASE TAMBIÉN" -e "SEE ALSO")
        if [ "$comando" == "" ]; then
            echo "ERROR:   No SEE-ALSO    for $1" >> $2
            sa="warning"
        fi
    fi

    # Write to results file
    if [ "$value" == 0 ]; then
        echo "Fail $1 $n $s $d $sa" >> $3
    else
        echo "Ok   $1 $n $s $d $sa" >> $3
    fi
    return
}


# no path as argument?
if [[ -z $1 ]]; then
    comandos=$(compgen -c)
else
    comandos=$(ls $1 -p | grep -v /)
fi

e="errors_$(date +%a-%d-%b-%H.%M.%S).txt"
r="results_$(date +%a-%d-%b-%H.%M.%S).txt"
echo $r
for i in $comandos;
do
    probarComando $i $e $r
done
echo "FILE_END" >> $r

