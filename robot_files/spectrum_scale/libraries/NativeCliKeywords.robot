*** Keywords ***

Native CLI UnixCommand
    [Documentation]    Runs a unix command
    [Arguments]        ${Params}    ${Target}=@{PROTOCOL_SERVER_NODES}[0]    ${ConnectionMode}=Credentials    ${Alias}=${EMPTY}    ${Username}=${USER}    ${Password}=${PASSWORD}    ${SSHKey}=${SSH_KEY}    ${DebugLevel}=INFO    ${ExpectToFail}=No    ${FailMessage}=None    ${SuccessMessage}=None    ${Mandatory}=Yes    ${useSudo}=${EMPTY}    ${Timeout}=${EMPTY}    ${Background}=No
    ${cline}=    
    ...        BuiltIn.Catenate    ${Params}

    BuiltIn.Log    Command to be executed is: ${cline}    level=INFO    html=False    console=False    repr=False
    @{output}=    
    ...        connectionKeywords.Connect And Execute    ${cline}    Target=${Target}    ConnectionMode=${ConnectionMode}    Alias=${Alias}    Username=${Username}    Password=${Password}    SSHKey=${SSHKey}    DebugLevel=${DebugLevel}    Timeout=${Timeout}    useSudo=${useSudo}    Background=${Background}

    BuiltIn.Run Keyword If    '${Background}' == 'No'
    ...        connectionKeywords.Analyze Result    rc=@{output}[0]    stdout=@{output}[1]    stderr=@{output}[2]    ShouldItFail=${ExpectToFail}    FailReturnMessage=${FailMessage}    SuccessReturnMessage=${SuccessMessage}    Mandatory=${Mandatory}    ${EMPTY}
    [Return]           @{output}


Native CLI GPFSCommand
    [Documentation]    Run a gpfs command found in the ${GPFS_COMMANDS_PATH} directory
    [Arguments]        ${Params}    ${Target}=@{PROTOCOL_SERVER_NODES}[0]    ${ConnectionMode}=Credentials


Native CLI core.snap.py
    [Documentation]    ${EMPTY}
    [Arguments]        ${Params}=${EMPTY}    ${Target}=@{PROTOCOL_SERVER_NODES}[0]


Test001
    [Documentation]    ${EMPTY}
    [Arguments]        algo2=algo2    algo3=algo3
    NativeCliKeywords.Test001    algo2=algo2    algo3=algo3


Saving Demo
    [Documentation]    ${EMPTY}
    sudoWrapperKeywords.Sudo Wrapper Setup    ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}


After all this time
    [Documentation]    Always
    XML.Add Element    ${EMPTY}    ${EMPTY}    index=None    xpath=.


After all this time
    [Documentation]    Always
    XML.Add Element    ${EMPTY}    ${EMPTY}    index=None    xpath=.


