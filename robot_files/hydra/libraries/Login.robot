*** Keywords ***

LoginDemo0510
    [Documentation]    Test to verify robot is working
    [Arguments]        ${SERVER}    ${GRID_ID}    ${USER}=admin    ${PWD}=admin
    Selenium2Library.Set Selenium Implicit Wait    20
    Selenium2Library.Set Selenium Timeout    20
    Selenium2Library.Set Selenium Speed    4
    ${LOGIN_URL}=    
    ...        BuiltIn.Set Variable    http://${SERVER}

    ${WELCOME URL}=    
    ...        BuiltIn.Set Variable    //${SERVER}/${GRID_ID}

    Selenium2Library.Title Should Be    ${EMPTY}
    Selenium2Library.Title Should Be    ${EMPTY}
    ${STATUSBROWSER}=    
    ...        BuiltIn.Run Keyword And Return
    ...        XML.Element Attribute Should Be    ${EMPTY}    ${EMPTY}    ${EMPTY}    xpath=xpath=//div[@class='login']    message=None

    BuiltIn.Run Keyword If    '${STATUSBROWSER}' == 'False'
    ...        Selenium2Library.Open Browser    ${LOGIN_URL}    browser=firefox    alias=None    remote_url=False    desired_capabilities=None    ff_profile_dir=None
    BuiltIn.Run Keyword If    '${STATUSBROWSER}' == 'False'
    ...        Selenium2Library.Maximize Browser Window
    Selenium2Library.Title Should Be    IBM TS7700
    Selenium2Library.Input Text    id=user    ${USER}
    Selenium2Library.Input Text    id=password    ${PWD}
    Selenium2Library.Click Element    xpath=//a[@id='submitBtn']
    Selenium2Library.Capture Page Screenshot    filename=filename=Login-Screenshot.png




