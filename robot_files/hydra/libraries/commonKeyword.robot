*** Keywords ***

getConnectionID
    [Documentation]    Returns the connection ID for the given cluster name.
    [Arguments]        ${Cluster}
    ${C_Values}=    
    ...        Collections.Get From Dictionary    ${Clusters_Dic}    ${Cluster}

    @{CLUSTER_VALUES}=    
    ...        String.Split String    ${C_Values}    separator=None    max_split=-1

    [Return]           ${CLUSTER_VALUES[0]}




