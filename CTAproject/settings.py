"""
Django settings for CTAproject project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from django import VERSION

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
APPNAME = os.path.basename(BASE_DIR)

ADMINS = (
    # ('Arturo Javier Lopez Fausto', 'ajlofa@mx1.ibm.com'),
    ('Arturo Javier Lopez Fausto', 'ricardo.ivan91@gmail.com'),
)

MEDIA_ROOT = BASE_DIR + '/media/'
MEDIA_URL = '/media/'

STATIC_ROOT = BASE_DIR + '/static/'
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    "static/apps/",
]

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'o5s3yksc#ryn(a13_*&-)^@x_y%fj$n4#fky24ab%)z-395a@b'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = False

TEMPLATE_DIRS = (
    # BASE_DIR
    os.path.join(BASE_DIR, 'templates'),
)

ALLOWED_HOSTS = []
# ALLOWED_HOSTS = ['9.18.3.97']

# Model for specifying additional information for users
AUTH_PROFILEAPPNAME = 'IBMauth.models.Contact'

# Authentication backend classes to use when attempting to authenticate a user
AUTHENTICATION_BACKENDS = (
    'IBMauth.backends.DummyBackend',
    'IBMauth.backends.LDAPBackend',
    # 'django.contrib.auth.backends.ModelBackend',
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'IBMauth',
    'CTAFramework',
    'testing',
    'django_extensions',
    'debug_toolbar'
)

MIDDLEWARE_CLASSES = (
    'IBMauth.middleware.BlockedIpMiddleware',
    'IBMauth.middleware.BlockedUserAgentsAdminMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

if VERSION[0] == 1 and VERSION[1] >= 7:
    MIDDLEWARE_CLASSES += (
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    )

ROOT_URLCONF = 'CTAproject.urls'

WSGI_APPLICATION = 'CTAproject.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ctaframework',
        'USER': 'postuser',
        'PASSWORD': 'Passw0rd',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

# Email settings
DEFAULT_FROM_EMAIL = "admin@ctaproject.gdl.ibm.com"  # % APPNAME
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_HOST = 'na.relay.ibm.com'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_SUBJECT_PREFIX = "CTA "  # % PROGRAM
SEND_BROKEN_LINK_EMAILS = False

LOGIN_URL = '/admin/login/'

try:
    from localsettings import *
except:
    pass
