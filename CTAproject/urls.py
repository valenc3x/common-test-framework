from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import IBMadmin as admin

urlpatterns = patterns(
    '',
    url(r'', include('CTAFramework.urls')),
    url(r'^api/', include('testing.api_urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT
    }),
    url(r'', include('testing.urls')),
)
